class Config {
  
  private JSONObject json;
  private int port;
  public String ip, dmx;
  private float lightMin,lightMax;  // Light setting
  private float totalMin, totalMax; // Total ranges
  private float songMin;            // The treshold of song
  private int centerRadius;         // Song treshold
  private JSONArray outputs;        // Definition of outputs
  private int width, height;        // The canvas dimensions
  private boolean debug;

  Config( String settings_ ){
    this.json = loadJSONObject(settings_);
    this.ip = json.getString("ip");
    this.dmx = json.getString("dmx");
    this.port = json.getInt("port");
    this.lightMin = json.getFloat("lightMin");
    this.lightMax = json.getFloat("lightMax");
    this.centerRadius = json.getInt("centerRadius");
    this.totalMin = json.getFloat("totalMin");
    this.totalMax = json.getFloat("totalMax");
    this.songMin = json.getFloat("songMin");
    this.outputs = json.getJSONArray("outputs");
    this.debug = json.getInt("debug") == 1 ? true : false;
    
    // Dimensions
    this.width = json.getInt("width");
    this.height = json.getInt("height");
    
  }
  
  /* Getters */
  
  public int port(){ return this.port; }
  
  public String ip(){ return this.ip; }
 
  public String dmx(){ return this.dmx; }
  
  public float lightMin(){ return this.lightMin; }
  
  public float lightMax(){ return this.lightMax; }
  
  public float totalMin() { return this.totalMin; }
  
  public float totalMax() { return this.totalMax; }
  
  public float songMin() { return this.songMin; }
  
  public int centerRadius(){ return this.centerRadius; }
  
  public int width(){ return this.width; }
  
  public int height(){ return this.height; }
  
  public boolean debug(){ return this.debug; }
  
  public JSONArray getOutputs(){ return this.outputs; }
  
}
