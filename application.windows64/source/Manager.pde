class Manager {
  private boolean camera, DMX, connected;
  private String USBDeviceName, cameraIP;
  private int frequency, frequencyHolder, cameraPort;
  
  Manager( int freqeuncy_ ) {
    this.camera = false;
    this.DMX = false;
    this.USBDeviceName = c.dmx();
    this.cameraIP = c.ip();
    this.cameraPort = c.port();
    this.frequency = freqeuncy_;
    this.frequencyHolder = 0;
  }
  
  private void debugDevice( String name_, boolean state_, int index_ ) {
    
    color col = state_ ? color( 0,255,0 ) : color(255,0,0);

    pushMatrix(); push();
    
    translate(0, index_ * (height/2));
    
    fill(col);
    stroke(0);
    rect(0,0,width, height/2);
    
    noStroke();
    fill(0);

    text(name_ + " " + (state_ ? "ON" : "OFF"),20,20);
    
    popMatrix(); pop();
    
  
  }
  
  // Routines
  public void testDevices(){
    
    this.frequencyHolder++;
  
    if ( this.frequencyHolder >= this.frequency ) {
      
      this.frequencyHolder = 0;
      
      if ( !this.ready() ) {
        this.debugDevice("DMX at " + c.dmx(), this.DMX(), 0);
        this.debugDevice("Camera at " + c.ip(), this.camera(), 1);
      }
      
      // println( frameCount + " oveřuji zařízení /////////////////////////////" );
      
      this.testDMX();
      this.testCamera();
      
      // println("Připraveno: " + this.ready() );
      // println("Připojeno: " + this.connected() );
      
      // println();
      
    }
    
    
    
  }
  
  // Tests
  public void testDMX(){
    
    boolean on = false;
    
    for ( String name : Serial.list() ) {
      if ( name.equals( this.USBDeviceName ) ) {
        on = true;
      }
    }
    this.setDMXState( on );
    if ( on == false ) {
      this.unconnect();
    }
    
    // println( "MANAGER dmx " + ( on ? "ON" : "DOWN" ) );
  
  }
  public void testCamera(){
    
    if ( !this.camera() ) {
    
      try {
      
        Socket socket = new Socket();
        socket.connect( new InetSocketAddress( this.cameraIP, this.cameraPort), 1000 );
        // println("MANAGER: kamera ON");
        this.setCameraState( true );
        
      } catch (IOException e){
        
        // println("MANAGER: kamera OFF");
        this.unconnect();
        this.setCameraState( false );
        
      }
    
    }
    
  }
  
  // Seters
  public void setDMXState( boolean state_ ){
    
    this.DMX = state_;
    
  }
  public void setCameraState( boolean state_ ){
    
    this.camera = state_;
    
  }
  
  // Getters
  public boolean camera() {
    return this.camera;
  }
  public boolean DMX(){
    return this.DMX;
  }
  public boolean ready(){
    return this.DMX && this.camera ? true : false;
  }
  public boolean connected(){
    return this.connected;
  }
  
  /* Actions */
  
  /* Connect devices */
  public void connect(){
    
    if ( !this.connected() ) {
      
      // Connect the RTCP
      if ( connection == null ) {
        // println("Spouštím konstruktor RTSP() je spouštěn");
        connection = new RTSP( main );
      }
      
      // Connect the DMX
      if ( dmx != null ) {
        dmx = new DmxP512( main, universeSize, false );
        dmx.setupDmxPro( DMXPRO_PORT, DMXPRO_BAUDRATE );
      }
      
      // set the connectivity state
      this.connected = true;
      
      // nastavit periferie
      this.mountPeriferies();
      
    }
    
  }
  
  /* Unconnect devices */
  public void unconnect(){

    this.connected = false;
    
    // unconnect the DMX
    dmx = null;
    
    // unconnect the IP camera
    connection = null;
    
    // unmount periferies
    periferies = new Output[0];
  }
  
  /* Mount periferies */
  public void mountPeriferies() {
  
    // The Outputs config
    
    // println( "mountuji periferie ////////////////////////////////////////////////////////////" );
    
    JSONArray outs = c.getOutputs();
    
    periferies = new Output[ outs.size() ];
    
    // Array of the periferies
      
    if ( outs.size() > 0 ) {
    
      for ( int i=0; i<outs.size();i++ ) {
        JSONObject output = outs.getJSONObject(i);
        
        switch ( output.getString("type") ){
        
          case "display":
            periferies[i] = new DisplayOutput( output );
            break;
          case "dmx":
            periferies[i] = new DMXOutput( output );
            break;
          case "dmxHead":
            periferies[i] = new DMXHeadOutput( output );
            break;
          case "sound":
            periferies[i] = new SoundOutput( output );
            break;
          case "synth":
            periferies[i] = new SynthOutput( output );
            break;
          case "song":
            periferies[i] = new SongOutput( output );
            break;
          case "debug":
            periferies[i] = new DebugOutput( output );
            break;
        
        }
      }
      
    }
    
    // println("Celkem jsem namountoval " + periferies.length + " periferií.");
  
  }
}
