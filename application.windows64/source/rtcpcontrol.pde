import processing.net.*;
import processing.serial.*;
import dmxP512.*;
import ddf.minim.*;
import ddf.minim.ugens.*;
import ddf.minim.effects.*;
import java.net.*;

DmxP512 dmx;
int universeSize=128;

boolean DMXPRO = true;
String DMXPRO_PORT = "COM3";
int DMXPRO_BAUDRATE = 115000;

boolean isSinging = false;
int numChantsSinging = 0;
float heartAspect = 1f;

PApplet main;

Config c;
RTSP connection;
Output[] periferies;

SampleDatabase pam;
SampleDatabase songs;

Manager m;

Minim minim;
AudioOutput dac;


void setup(){
  
  main = this;
  
  size(600,600);
  surface.setTitle("Zářivý úsměv dívky s kyticí - spouštěč");
  surface.setLocation(50, 50);
  
  // The settings
  c = new Config("settings.json");
  
  // The devices manager
  m = new Manager(5*60);
  
  // trigger the database
  pam = new SampleDatabase(8, "pim");
  songs = new SampleDatabase(5, "songs");
  
  
  // The connections are null before you get started
  connection = null;
  dmx = null;
  
  // The DMX universe
  // dmx = new DmxP512( this, universeSize, false );
  //dmx.setupDmxPro( DMXPRO_PORT, DMXPRO_BAUDRATE );
  
  // The sound
  minim = new Minim( this );
  dac = minim.getLineOut();
  
  
  // Mount the periferies to a global variable
  periferies = new Output[0];
  
  
}

void draw(){
  
  if (frameCount == 30){
    pam.loadFiles();
    songs.loadFiles();
  }
  
  // test devices
  m.testDevices();
  
  // Perform all updates on updated devices
  if ( m.connected() ) {
    
    // redraw
    background(0);
    
    // Update the connection
    if ( connection != null ) {
      
      connection.update();
      
      for (int i=0;i<periferies.length;i++){
        
          periferies[i].update();
        
      }
      
    }
    
    
    
  } else if ( m.ready() && ! m.connected() ) {
    m.connect();
  }
  
  
  
  
}

void clientEvent( Client someClient ){
  
  if ( m.ready() ) {
    connection.listen();
  }
  
}
