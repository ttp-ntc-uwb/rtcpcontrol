class DMXFixture {

  JSONObject setting;
  int channel;
  
  DMXFixture( JSONObject setting_ ){
  
    this.setting = setting_;
    this.channel = this.setting.getInt("channel");
  
  }
  
  public void sendColor( color col ){
    
    if ( dmx != null ) {
    
      
      for ( int i=0; i<this.setting.getJSONArray("message").size();i++ ) {
        JSONObject row = this.setting.getJSONArray("message").getJSONObject(i);
        
        if ( row.getString("type").equals("color") ) {
          
          int v = 0;
        
          switch ( row.getString("val") ){
            case "r":
              v = (int) red(col);
              break;
            case "g":
              v = (int) green(col);
              break;
            case "b":
              v = (int) blue(col);
              break;
          }
          
          dmx.set( this.channel + i, v );
          
        } else if ( row.getString("type").equals("brightness") ) {
          dmx.set( this.channel + i, (int) brightness(col) );
        } else {
          dmx.set( this.channel + i, row.getInt("val") );
        }
        
      }
      
    
    }
  
    
    
  }

}
