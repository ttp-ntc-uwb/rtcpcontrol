class RTSP {

  Client cli;
  private boolean streaming, pingScheduled;
  private int streamingTime, streamingInterval, streamingResolution;
  // private float streamigResolution;
  private String session;
  
  private ArrayList<RTSPState> states;
  private int listeningForState;
  private int listeningForCSeq;
  
  // Incoming stream
  private boolean rtpReadsData;
  private int rtpReadCount;
  private byte[] rtpBuffer;
  
  // New incoming stream
  private int rtpFrameRead;
  private byte[] rtpFrameBuffer;
  private boolean rtpFrameHasStarted;
  private int frameDuration, frameDurationLimit;
  
  // The result output
  // private int[] mappedBuffer;
  // private int average, min, max, focal;
  
  // The light output
  private int[] lightBuffer;
  
  // The total output
  private float[] totalBuffer;
  private float totalMin, totalMax, totalAverage, totalFocal, totalPan;
  
  RTSP( PApplet main_ ){
    
    println("RTSP konstruktor uvnitř");
  
    // RTSP connection
    this.cli = new Client(main_, c.ip(), c.port());
    
    // RTSP setup
    this.streaming = false;
    this.pingScheduled = false;
    this.listeningForState = 0;
    this.listeningForCSeq = 0;
    this.session = null;
    this.streamingTime = 0;
    this.streamingResolution = 30;
    this.streamingInterval = 120;
    
    // Setup of states
    this.states = new ArrayList<RTSPState>();
    
    // Incoming RTP
    this.rtpReadsData = false;
    this.rtpBuffer = new byte[0];
    this.rtpReadCount = 0;
    
    this.frameDuration = 0;
    this.frameDurationLimit = 6*60;
    
    // Mapped settings
    // this.mappedBuffer = new int[0];
    this.lightBuffer = new int[0];
    this.totalBuffer = new float[0];
    
    // Configure the first state
    RTSPState s1 = new RTSPState( 0, c.ip(), c.port(), "OPTIONS" );
    s1.setURI("thermal.sdp");
    
    // Configure the second state
    RTSPState s2 = new RTSPState( 1, c.ip(), c.port(), "DESCRIBE" );
    s2.setURI("thermal.sdp");
    
    // Configure the third state
    RTSPState s3 = new RTSPState( 2, c.ip(), c.port(), "SETUP" );
    s3.setURI("thermal.sdp");
    s3.setLine("Transport", "RTP/AVP/TCP;unicast;interleaved=0-1");
    
    // Configure the fourth state
    RTSPState s4 = new RTSPState( 3, c.ip(), c.port(), "PLAY" );
    s4.setURI("thermal.sdp");
    s4.setLine("Session", "RTP/AVP/TCP;unicast;interleaved=0-1");
    
    // The ping
    RTSPState s5 = new RTSPState( 4, c.ip(), c.port(), "OPTIONS" );
    s5.setURI("thermal.sdp");
    s5.setLine("Session", this.session );
    
    // Set parameter
    RTSPState s6 = new RTSPState( 5, c.ip(), c.port(), "SET_PARAMETER" );
    s6.setURI("thermal.sdp");
    s6.setLine("Content-Type", "text/parameters" );
    int contentLength = ("interface.palette.scale.min:" + c.lightMin() + "\ninterface.palette.scale.max" + c.lightMax() + "\n").length();
    s6.setLine( "Content-Length", str(contentLength) );
    s6.setLine( "interface.palette.scale.min", str( c.lightMin() ) );
    s6.setLine( "interface.palette.scale.max", str( c.lightMax() ) );

    
    
    // Append all states to the ArrayList
    this.states.add( s1 );
    this.states.add( s2 );
    this.states.add( s3 );
    this.states.add( s4 );
    this.states.add( s5 );
    this.states.add( s6 );
    
    this.cli.write( s1.getMessage() );
    
  }
  
  /* Routines */
  
  /* Updates each turn */
  void update() {
    
    if ( this.streaming ) {
      
      int pingFreq = 30 * 60;
      
      if ( this.streamingTime >= pingFreq ) {
        
          this.cli.write( this.states.get(4).getMessage() );
          
          this.states.get(4).increaseCseq();
          
          this.streamingTime = 0;
      
      } else {
        
        this.streamingTime++;

      }
      
      // Detekuje delay mezi jednotlivými framy
      // Timeout checking
      if ( this.streaming ) {
        this.frameDuration++;
      }
      
      if ( this.frameDuration >= this.frameDurationLimit ) {
          m.unconnect();
          m.setCameraState(false);
        }
      
    }
  
  }
  
  /* Listening to server messages */
  void listen(){
  
    // Proceed only when the serial has sent any input
    if ( this.cli.available() > 0 ) {
       
      // 1: Streaming setup
      if ( ! this.streaming ) {
        
        // 
        String msg = this.cli.readString();
        int msgCseq = RTCPParser.cseq( msg );
        int msgStatus = RTCPParser.status( msg );
        
        // Continue only on OK messages corresponding to current state and CSeq
        if ( 
          msgStatus == 200
          && msgCseq == this.listeningForCSeq
        ) {
          
          println( "Úspěšně jsem obdržel zprávu:" );
          println(msg);
          println( "Nyní čekám na " + this.listeningForState );
          println( "////////////" );

          // Control if the message corresponds to the current state
          switch ( this.listeningForState ) {
          
            case 0:
              if ( RTCPParser.getStringValueAt("Public", msg).contains("OPTIONS, DESCRIBE, PLAY, PAUSE, SETUP, TEARDOWN") ) {
                // The status 0 is successfull
                this.listeningForState = 1;
                this.listeningForCSeq = this.states.get(1).cseq();
                String message = this.states.get(1).getMessage();
                this.cli.write( message );
              }
              break;
            case 1:
              if ( RTCPParser.getStringValueAt("Content-Type", msg).contains("application/sdp") ) {
                
                this.listeningForState = 2;
                this.listeningForCSeq = this.states.get(2).cseq();
                this.cli.write( this.states.get(2).getMessage() );
              
              }
              break;
            case 2:
              if ( RTCPParser.getStringValueAt("Session", msg) != null ) {
                println(RTCPParser.getStringValueAt("Session", msg));
                
                this.session = RTCPParser.getStringValueAt("Session", msg);
                this.listeningForCSeq = this.states.get(3).cseq();
                this.listeningForState = 3;
                this.states.get(3).setLine("Session", this.session );
                this.states.get(4).setLine("Session", this.session );
                
                this.cli.write( this.states.get(3).getMessage() );
              }
              break;
              /*
            case 3:
            println( this.states.get(5).getMessage() ); 
              if ( RTCPParser.getStringValueAt("Content-Type", msg) != null ) {
                // println(RTCPParser.getStringValueAt("Session", msg));
                
                this.session = RTCPParser.getStringValueAt("Session", msg);
                this.listeningForCSeq = this.states.get(5).cseq();
                this.listeningForState = 5;
                // this.states.get(5).setLine("Session", this.session );
                
                
                
                this.cli.write( this.states.get(5).getMessage() );
              }
              break;
              */
            case 3:
            println( msgCseq );
              if ( msgCseq == 3 ) {
                println("Mohu běžet!");
                this.streaming = true;
              }
              break;
            
          }
        
        } 
        // Debug setup output
        else {
        
          println("Setting up sequence at phase " + this.listeningForState +": GOT ERROR:");
          println(msg);
          if ( RTCPParser.getStringValueAt("Session", msg) != null ) {
            if ( RTCPParser.getStringValueAt( "Public", msg ) != null ) {
              println( "Přijal jsem Ping" );
              this.streaming = true;
              
              // Clear the buffer
              this.cli.clear();
            }
          }
          
        }
        
      } // End setting up sequence
      
      // 2: Streaming itself
      else {        
        
        // Uloží celou zprávu do bufferu
        byte[] message = this.cli.readBytes();
        this.cli.clear();
        
        // pokud začíná nový frame, čti tento frame
        // 
        if ( message[0] == 'R' && message[1] == 'T' && message[2]=='S' ) {
          println("XXXXXXXXXXXX přijímám hlavičku odpovědi na ping //////////////////////////////////////");
        } else if ( message[0] == '$' ) {
          this.rtpFrameHasStarted = true;
          this.rtpFrameRead = 0;
          int size = (int)(message[2] << 8 | message[3]) & 0xFFFF;
          
          // Ofset = 17-18
          int offset = binToInt( message[17], message[18] );
          
          this.rtpFrameBuffer = new byte[ message.length - 19 ];
          
          int bCount = 0;
          for ( int i = 19; i<message.length; i++ ) {
            this.rtpFrameBuffer[bCount] = message[i];
            bCount++;
          }
          
          this.processFrame();
          
        } 
        if ( message[0] != '$' && message[0] != 'R' ) {
          println("ostatní stream");
        }
        
        this.frameDuration = 0;
        
        
        
        
        
        
        
        /*
        
        // Pokud nečtu čtu data, čekám na hlavičku
        // hlavička začíná znakem $
        if ( ! this.rtpReadsData ) {
          
          // načíst první 4 byty, tedy hlavičku
          byte[] header = this.cli.readBytes( 4 ); 
          
          // zkontrolovat, zda se jedná o začátek hlavičky
          if ( header[0] == '$' ) {
            
            
            int size = (int)(header[2] << 8 | header[3]) & 0xFFFF;
            
            // Processovat buffer
            if ( this.rtpBuffer.length > 0 ) {
              this.processBuffer();
            }
            
            
            
            // Reinicializovat buffer
            this.rtpBuffer = new byte[size];
            //// this.rtpReadCount = 0;
            
            // Uložit zbytek data do bufferu
            this.rtpReadCount = this.cli.readBytes(this.rtpBuffer);
            // this.rtpReadCount = header.length - 30;
            
            // Vyčistí buffer do nuly
            this.cli.clear();
            
            // začít číst data
            this.rtpReadsData = true;
            
            
          }
        
        }
        
        // Pokud čtu data, ukládám celou zprávu do bufferu
        else {
          
          
          
          // Vytvořit si prozatimní buffer
          byte[] bufferTmp = this.cli.readBytes( this.rtpBuffer.length - this.rtpReadCount );
          
          // println( bufferTmp[0] );
          
          for ( int i=0; i<bufferTmp.length; i++ ) {
            byte b = bufferTmp[i];
            this.rtpBuffer[ i + this.rtpReadCount ] = b;
          }
          
          this.rtpReadCount += bufferTmp.length;
          
          if ( this.rtpReadCount == this.rtpBuffer.length ) {
          
            this.rtpReadsData = false;
            
            // println( "Teď mi končí frame" );
            if ( this.pingScheduled == true ) {
              
              println(frameCount + " /////////////////////////////////////////");
            
              this.pingScheduled = false;
            
              this.streaming = false;
          
              this.cli.write( this.states.get(4).getMessage() );
          
              this.states.get(4).increaseCseq();
          
              this.listeningForState = 9;
            
            }
            
            
          }
          
          
        }
        
        */
        
        
        // Pokud nečtu data, čekám na hlavičku
        
          // Hlavička je 4 bajty dlouhá
          
            // Pokud 1. bajt == $ >>>>> hlavička začíná
            
            // Zjistit velikost správy a vytvořit prázdný pole o této velikosti to this.buffer
            
            // this.rtcpReadData = 0
            // this.buffer = new byte[size];
            
            // int length = this.cli.readBytes( this.buffer );
            // this.readData += length;
        
        // pokud čtu data, zapisuju do bufferu
        
          // byte[] bufferTmp = this.cli.readBytes( this.buffer.length - this.readData );
          
          // for ( byte b : bufferTmp ) {
            // this.buffer[ this.readData + indexV Rámci Smyčky Začínající od nuly ] = b;
          // }
          
          // this.readData += bufferTmp.length
          
          // if ( this.readData = this.buffer.length )  {this.readsData = true; }        
      
      } // End of streaming loop
    
    }
  
  }
  
  
  /* Setters */
  
  private void processFrame(){
    byte[] buf = new byte[ this.rtpFrameBuffer.length ];
    arrayCopy( this.rtpFrameBuffer, buf );
    // println("Zpracovávám buffer");
    // println( buf.length / 2 );
    // println(  );
    
    // Reinicializovat kelvinový buffer
    int numPixels = (int) ( buf.length / 2 );
    this.totalBuffer = new float[ numPixels ];
    this.lightBuffer = new int[ numPixels ];
    
    // Reinicializace proměnných
    this.totalMin = 9999;
    this.totalMax = -9999;
    this.totalAverage = 0;
    this.totalPan = 0;
    
    // temporary variables
    float totalFocalSummer = 0;
    int totalFocalPxCount = 0;
    float totalAverageSummer = 0;
    
    int pxCount = 0;
    
    for ( int i = 0; i < buf.length; i +=2 ) {
    
      int raw = (( buf[i+1] & 0xFF )<< 8 ) | (buf[i] & 0xFF);
      
      // Unit conversion
      float kelvin = raw/100f;
      float celsius = kelvin - 273.15;
      
      // trim celsius in total range
      celsius = this.celsiusInTotalRange( celsius );
      
      // Calculate the coordinates of the pixel
      int x = pxCount % c.width();
      int y = (int) pxCount / c.width();
      
      // Calculate the absolute focal value
      if ( x > c.width()/2 - c.centerRadius() && x < c.width()/2 + c.centerRadius() ) {
      
        if ( y > c.height()/2 - c.centerRadius() && y < c.height()/2 + c.centerRadius() ) {
        
          totalFocalSummer += celsius;
          totalFocalPxCount++;
        
        }
      
      }
      
      // Total average
      totalAverageSummer += celsius;
      
      // Total minimum & maximum
      if ( celsius < this.totalMin ) { this.totalMin = celsius; }
      if ( celsius > this.totalMax ) { this.totalMax = celsius; }
      
      // Total buffer assignement
      this.totalBuffer[pxCount] = celsius;
      
      // Light buffer assignment
      this.lightBuffer[pxCount] = (int) map( this.celsiusInLightRangeFloat(celsius), c.lightMin(), c.lightMax(), 0, 255 );
      
      pxCount++; 
    
    }
    
    this.totalAverage = totalAverageSummer / pxCount;
    this.totalFocal = totalFocalSummer / totalFocalPxCount;
    
    // Calculate the pan
    float panTreshold = this.totalAverage; // lerp( this.totalAverage, c.totalMax(), 0.5 );
    int panSummer = 0;
    int panCount = 0;
    for ( int i=0;i<this.totalBuffer.length;i++ ) {
      
      if ( this.totalBuffer[i] > panTreshold ) {
      
        panSummer += i % c.width();
        panCount++;
      
      }
      
    }
    
    if ( panCount == 0 ) {
      this.totalPan = 0;
    } else {
      this.totalPan = map( panSummer/panCount, 0,c.width(), -1, 1 );
    }
    
    
    
  }
  
  /* Process the buffer */
  private void processBuffer(){
  
    byte[] buf = new byte[ this.rtpBuffer.length ];
    arrayCopy( this.rtpBuffer, buf );
    
    // println( "Délka bufferu " + this.rtpBuffer.length );
    
    // Zjistit si offset, který je v bufferu na místě 17-18
    int offset = binToInt( buf[13], buf[14] );
    
    
    // Reinicializovat si buffer
    int numPixels = ( buf.length - 15 ) / 2;
    
    this.totalBuffer = new float[ numPixels ];
    
    // nová reinicializace proměnných
    this.lightBuffer = new int[ numPixels ];
    this.totalBuffer = new float[ numPixels ];
    this.totalMin = 9999;
    this.totalMax = -9999;
    this.totalAverage = 0;
    this.totalPan = 0;
    
    // temporary variables
    float totalFocalSummer = 0;
    int totalFocalPxCount = 0;
    float totalAverageSummer = 0;
    
    
    // PVector center = new PVector( c.width()/2, c.height()/2 );

    // Procesovat buffer
    int pxCount = 0;
    
    for ( int i = 15; i < buf.length; i +=2 ) {
      
      // Convert the bytes to integer value
      int raw = (( buf[i+1] & 0xFF )<< 8 ) | (buf[i] & 0xFF);
      
      // Unit conversion
      float kelvin = raw/100f;
      float celsius = kelvin - 273.15;
      
      // Trim the celsius in the working range
      celsius = this.celsiusInTotalRange( celsius );
      
      // Calculate the coordinates of the pixel
      int x = pxCount % c.width();
      int y = (int) pxCount / c.width();
      
      // Calculate the absolute focal value
      if ( x > c.width()/2 - c.centerRadius() && x < c.width()/2 + c.centerRadius() ) {
      
        if ( y > c.height()/2 - c.centerRadius() && y < c.height()/2 + c.centerRadius() ) {
        
          totalFocalSummer += celsius;
          totalFocalPxCount++;
        
        }
      
      }
      
      // Total average
      totalAverageSummer += celsius;
      
      // Total minimum & maximum
      if ( celsius < this.totalMin ) { this.totalMin = celsius; }
      if ( celsius > this.totalMax ) { this.totalMax = celsius; }
      
      // Total buffer assignement
      this.totalBuffer[pxCount] = celsius;
      
      // Light buffer assignment
      this.lightBuffer[pxCount] = (int) map( this.celsiusInLightRangeFloat(celsius), c.lightMin(), c.lightMax(), 0, 255 );
      
      pxCount++;
      
    }
    
    //
    
    this.totalAverage = totalAverageSummer / pxCount;
    this.totalFocal = totalFocalSummer / totalFocalPxCount;
    
    // Calculate the pan
    float panTreshold = this.totalAverage; // lerp( this.totalAverage, c.totalMax(), 0.5 );
    int panSummer = 0;
    int panCount = 0;
    for ( int i=0;i<this.totalBuffer.length;i++ ) {
      
      if ( this.totalBuffer[i] > panTreshold ) {
      
        panSummer += i % c.width();
        panCount++;
      
      }
      
    }
    
    if ( panCount == 0 ) {
      this.totalPan = 0;
    } else {
      this.totalPan = map( panSummer/panCount, 0,c.width(), -1, 1 );
    }

  }
  
  /* Converters */
  
  /* Trim celsius in the total range */
  private float celsiusInTotalRange( float input ){
    if ( input < c.totalMin() ) { input = c.totalMin(); }
    if ( input > c.totalMax() ) { input = c.totalMax(); }
    return input;
  }
  
  /* Trim the celsius in light range */
  private int celsiusInLightRangeInt( float input ){
    if ( input < c.lightMin() ) { input = c.lightMin(); }
    if ( input > c.lightMax() ) { input = c.lightMax(); }
    return (int) input;
  }
  
  private float celsiusInLightRangeFloat( float input ){
    if ( input < c.lightMin() ) { input = c.lightMin(); }
    if ( input > c.lightMax() ) { input = c.lightMax(); }
    return input;
  }
  
  /* Getters */
  
  /* Get the entire data */
  public float[] getData(){ return this.totalBuffer; }
  
  public int[] getLightData(){ return this.lightBuffer; }
  
  public float getAverage(){ return constrain( this.totalAverage, c.totalMin(), c.totalMax() ); }
  
  public int getLightAverageInt() { return (int) constrain( this.celsiusInLightRangeInt( this.totalAverage) , c.totalMin(), c.totalMax() ); }
  
  public float getLightAverageFloat() { return constrain( this.celsiusInLightRangeFloat( this.totalAverage ), c.totalMin(), c.totalMax() ); }
  
  public float getMin(){ return constrain( this.totalMin, c.totalMin(), c.totalMax()) ; }
  
  public int getLightMinInt(){ return this.celsiusInLightRangeInt( this.totalMin ); }
  
  public float getLightMinFloat(){ return this.celsiusInLightRangeFloat( this.totalMin ); }
  
  public float getMax(){ return this.totalMax; }
  
  public int getLightMaxInt(){ return this.celsiusInLightRangeInt( this.totalMax ); }
  
  public float getLightMaxFloat(){ return this.celsiusInLightRangeFloat( this.totalMax ); }
  
  public float getFocal(){ return constrain( this.totalFocal,c.totalMin(), c.totalMax()); }
  
  public int getLightFocalInt() { return (int) constrain( this.celsiusInLightRangeInt( this.totalFocal ), c.lightMin(), c.lightMax() ); }
  
  public float getLightFocalFloat() { return constrain( this.celsiusInLightRangeFloat( this.totalFocal ), c.lightMin(), c.lightMax() ); }
  
  public float getPan(){ return this.totalPan; }
  
}


int binToInt( byte left, byte right ){
  return (int) (left << 8 | right) & 0xFFFF;
}

short binToShort( byte left, byte right ){
  return (short) ( (right << 8 | left) & 0xFFFF );
}
