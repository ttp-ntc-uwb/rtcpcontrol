class SampleDatabase {

  MultiChannelBuffer buffers[];
  float rates[];
  String folder;
  
  SampleDatabase( int index, String folder_ ) {
  
    this.buffers = new MultiChannelBuffer[index];
    this.rates = new float[index];
    this.folder = folder_;
    
    for ( int i=0; i<this.buffers.length; i++ ) {
    
      
      this.buffers[i] = new MultiChannelBuffer( 9000, 2 );
      
    }
    
    
    
  }
  
  public void loadFiles(){
  
    for ( int i=0; i<this.buffers.length; i++ ) {
      String fileName = "data/" + this.folder + "/0"+(i+1)+".mp3";
      this.rates[i] = minim.loadFileIntoBuffer( fileName, this.buffers[i] );
      println(fileName);
      // minim.loadFileIntoBuffer( fileName, this.buffers[i] );
    }
    
  }
  
  public MultiChannelBuffer getBuffer( int index ){
    return this.buffers[index];
  }
  
  public float getSampleRate( int index ) {
    return this.rates[index];
  }
  
  public int getLength() {
    return this.buffers.length;
  }

}




class SampleInstrument implements Instrument {
  
  Sampler sampler;
  Gain gain;

  SampleInstrument( int index, SampleDatabase db ){
    
    this.sampler = new Sampler( db.getBuffer( index ), db.getSampleRate( index ), 1 );
    this.gain = new Gain( 0f );
    
    this.sampler.patch( gain );
    
  }
  
  public void noteOn(float volume) {
    
    volume = constrain( volume, -60f, 0f );
    
    this.gain.setValue( volume );
    
    this.gain.patch( dac );
    this.sampler.trigger();
    
  }
  
  public void noteOn(  float amp, int index ){
  
    this.sampler.unpatch( gain );
    this.gain.unpatch( dac );
    numChantsSinging++;
    
  }
  
  public void noteOff(){
  
    this.sampler.unpatch( gain );
    this.gain.unpatch( dac );
    numChantsSinging--;
  
  }

}

void keyPressed() {

  if (key=='c') {
    println("song");
    int index = (int) random(0,pam.getLength()-1);
    dac.playNote( 0, -3f, new SampleInstrument( index, pam ) );
  }
  
  if (key=='s') {
    println("song");
    int index = (int) random(0,songs.getLength()-1);
    dac.playNote( 0, -12f, new SampleInstrument( index, songs ) );
  }
  
}
