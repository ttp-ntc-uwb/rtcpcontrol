import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.net.*; 
import processing.serial.*; 
import dmxP512.*; 
import ddf.minim.*; 
import ddf.minim.ugens.*; 
import ddf.minim.effects.*; 
import java.net.*; 
import ddf.minim.javasound.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class rtcpcontrol extends PApplet {









DmxP512 dmx;
int universeSize=128;

boolean DMXPRO = true;
String DMXPRO_PORT = "COM3";
int DMXPRO_BAUDRATE = 115000;

boolean isSinging = false;
int numChantsSinging = 0;
float heartAspect = 1f;

PApplet main;

Config c;
RTSP connection;
Output[] periferies;

SampleDatabase pam;
SampleDatabase songs;

Manager m;

Minim minim;
AudioOutput dac;


public void setup(){
  
  main = this;
  
  
  surface.setTitle("Zářivý úsměv dívky s kyticí - spouštěč");
  surface.setLocation(50, 50);
  
  // The settings
  c = new Config("settings.json");
  
  // The devices manager
  m = new Manager(5*60);
  
  // trigger the database
  pam = new SampleDatabase(8, "pim");
  songs = new SampleDatabase(5, "songs");
  
  
  // The connections are null before you get started
  connection = null;
  dmx = null;
  
  // The DMX universe
  // dmx = new DmxP512( this, universeSize, false );
  //dmx.setupDmxPro( DMXPRO_PORT, DMXPRO_BAUDRATE );
  
  // The sound
  minim = new Minim( this );
  dac = minim.getLineOut();
  
  
  // Mount the periferies to a global variable
  periferies = new Output[0];
  
  
}

public void draw(){
  
  if (frameCount == 30){
    pam.loadFiles();
    songs.loadFiles();
  }
  
  // test devices
  m.testDevices();
  
  // Perform all updates on updated devices
  if ( m.connected() ) {
    
    // redraw
    background(0);
    
    // Update the connection
    if ( connection != null ) {
      
      connection.update();
      
      for (int i=0;i<periferies.length;i++){
        
          periferies[i].update();
        
      }
      
    }
    
    
    
  } else if ( m.ready() && ! m.connected() ) {
    m.connect();
  }
  
  
  
  
}

public void clientEvent( Client someClient ){
  
  if ( m.ready() ) {
    connection.listen();
  }
  
}
class Config {
  
  private JSONObject json;
  private int port;
  public String ip, dmx;
  private float lightMin,lightMax;  // Light setting
  private float totalMin, totalMax; // Total ranges
  private float songMin;            // The treshold of song
  private int centerRadius;         // Song treshold
  private JSONArray outputs;        // Definition of outputs
  private int width, height;        // The canvas dimensions
  private boolean debug;

  Config( String settings_ ){
    this.json = loadJSONObject(settings_);
    this.ip = json.getString("ip");
    this.dmx = json.getString("dmx");
    this.port = json.getInt("port");
    this.lightMin = json.getFloat("lightMin");
    this.lightMax = json.getFloat("lightMax");
    this.centerRadius = json.getInt("centerRadius");
    this.totalMin = json.getFloat("totalMin");
    this.totalMax = json.getFloat("totalMax");
    this.songMin = json.getFloat("songMin");
    this.outputs = json.getJSONArray("outputs");
    this.debug = json.getInt("debug") == 1 ? true : false;
    
    // Dimensions
    this.width = json.getInt("width");
    this.height = json.getInt("height");
    
  }
  
  /* Getters */
  
  public int port(){ return this.port; }
  
  public String ip(){ return this.ip; }
 
  public String dmx(){ return this.dmx; }
  
  public float lightMin(){ return this.lightMin; }
  
  public float lightMax(){ return this.lightMax; }
  
  public float totalMin() { return this.totalMin; }
  
  public float totalMax() { return this.totalMax; }
  
  public float songMin() { return this.songMin; }
  
  public int centerRadius(){ return this.centerRadius; }
  
  public int width(){ return this.width; }
  
  public int height(){ return this.height; }
  
  public boolean debug(){ return this.debug; }
  
  public JSONArray getOutputs(){ return this.outputs; }
  
}
class DMXFixture {

  JSONObject setting;
  int channel;
  
  DMXFixture( JSONObject setting_ ){
  
    this.setting = setting_;
    this.channel = this.setting.getInt("channel");
  
  }
  
  public void sendColor( int col ){
    
    if ( dmx != null ) {
    
      
      for ( int i=0; i<this.setting.getJSONArray("message").size();i++ ) {
        JSONObject row = this.setting.getJSONArray("message").getJSONObject(i);
        
        if ( row.getString("type").equals("color") ) {
          
          int v = 0;
        
          switch ( row.getString("val") ){
            case "r":
              v = (int) red(col);
              break;
            case "g":
              v = (int) green(col);
              break;
            case "b":
              v = (int) blue(col);
              break;
          }
          
          dmx.set( this.channel + i, v );
          
        } else if ( row.getString("type").equals("brightness") ) {
          dmx.set( this.channel + i, (int) brightness(col) );
        } else {
          dmx.set( this.channel + i, row.getInt("val") );
        }
        
      }
      
    
    }
  
    
    
  }

}
class SampleDatabase {

  MultiChannelBuffer buffers[];
  float rates[];
  String folder;
  
  SampleDatabase( int index, String folder_ ) {
  
    this.buffers = new MultiChannelBuffer[index];
    this.rates = new float[index];
    this.folder = folder_;
    
    for ( int i=0; i<this.buffers.length; i++ ) {
    
      
      this.buffers[i] = new MultiChannelBuffer( 9000, 2 );
      
    }
    
    
    
  }
  
  public void loadFiles(){
  
    for ( int i=0; i<this.buffers.length; i++ ) {
      String fileName = "data/" + this.folder + "/0"+(i+1)+".mp3";
      this.rates[i] = minim.loadFileIntoBuffer( fileName, this.buffers[i] );
      println(fileName);
      // minim.loadFileIntoBuffer( fileName, this.buffers[i] );
    }
    
  }
  
  public MultiChannelBuffer getBuffer( int index ){
    return this.buffers[index];
  }
  
  public float getSampleRate( int index ) {
    return this.rates[index];
  }
  
  public int getLength() {
    return this.buffers.length;
  }

}




class SampleInstrument implements Instrument {
  
  Sampler sampler;
  Gain gain;

  SampleInstrument( int index, SampleDatabase db ){
    
    this.sampler = new Sampler( db.getBuffer( index ), db.getSampleRate( index ), 1 );
    this.gain = new Gain( 0f );
    
    this.sampler.patch( gain );
    
  }
  
  public void noteOn(float volume) {
    
    volume = constrain( volume, -60f, 0f );
    
    this.gain.setValue( volume );
    
    this.gain.patch( dac );
    this.sampler.trigger();
    
  }
  
  public void noteOn(  float amp, int index ){
  
    this.sampler.unpatch( gain );
    this.gain.unpatch( dac );
    numChantsSinging++;
    
  }
  
  public void noteOff(){
  
    this.sampler.unpatch( gain );
    this.gain.unpatch( dac );
    numChantsSinging--;
  
  }

}

public void keyPressed() {

  if (key=='c') {
    println("song");
    int index = (int) random(0,pam.getLength()-1);
    dac.playNote( 0, -3f, new SampleInstrument( index, pam ) );
  }
  
  if (key=='s') {
    println("song");
    int index = (int) random(0,songs.getLength()-1);
    dac.playNote( 0, -12f, new SampleInstrument( index, songs ) );
  }
  
}
class Manager {
  private boolean camera, DMX, connected;
  private String USBDeviceName, cameraIP;
  private int frequency, frequencyHolder, cameraPort;
  
  Manager( int freqeuncy_ ) {
    this.camera = false;
    this.DMX = false;
    this.USBDeviceName = c.dmx();
    this.cameraIP = c.ip();
    this.cameraPort = c.port();
    this.frequency = freqeuncy_;
    this.frequencyHolder = 0;
  }
  
  private void debugDevice( String name_, boolean state_, int index_ ) {
    
    int col = state_ ? color( 0,255,0 ) : color(255,0,0);

    pushMatrix(); push();
    
    translate(0, index_ * (height/2));
    
    fill(col);
    stroke(0);
    rect(0,0,width, height/2);
    
    noStroke();
    fill(0);

    text(name_ + " " + (state_ ? "ON" : "OFF"),20,20);
    
    popMatrix(); pop();
    
  
  }
  
  // Routines
  public void testDevices(){
    
    this.frequencyHolder++;
  
    if ( this.frequencyHolder >= this.frequency ) {
      
      this.frequencyHolder = 0;
      
      if ( !this.ready() ) {
        this.debugDevice("DMX at " + c.dmx(), this.DMX(), 0);
        this.debugDevice("Camera at " + c.ip(), this.camera(), 1);
      }
      
      // println( frameCount + " oveřuji zařízení /////////////////////////////" );
      
      this.testDMX();
      this.testCamera();
      
      // println("Připraveno: " + this.ready() );
      // println("Připojeno: " + this.connected() );
      
      // println();
      
    }
    
    
    
  }
  
  // Tests
  public void testDMX(){
    
    boolean on = false;
    
    for ( String name : Serial.list() ) {
      if ( name.equals( this.USBDeviceName ) ) {
        on = true;
      }
    }
    this.setDMXState( on );
    if ( on == false ) {
      this.unconnect();
    }
    
    // println( "MANAGER dmx " + ( on ? "ON" : "DOWN" ) );
  
  }
  public void testCamera(){
    
    if ( !this.camera() ) {
    
      try {
      
        Socket socket = new Socket();
        socket.connect( new InetSocketAddress( this.cameraIP, this.cameraPort), 1000 );
        // println("MANAGER: kamera ON");
        this.setCameraState( true );
        
      } catch (IOException e){
        
        // println("MANAGER: kamera OFF");
        this.unconnect();
        this.setCameraState( false );
        
      }
    
    }
    
  }
  
  // Seters
  public void setDMXState( boolean state_ ){
    
    this.DMX = state_;
    
  }
  public void setCameraState( boolean state_ ){
    
    this.camera = state_;
    
  }
  
  // Getters
  public boolean camera() {
    return this.camera;
  }
  public boolean DMX(){
    return this.DMX;
  }
  public boolean ready(){
    return this.DMX && this.camera ? true : false;
  }
  public boolean connected(){
    return this.connected;
  }
  
  /* Actions */
  
  /* Connect devices */
  public void connect(){
    
    if ( !this.connected() ) {
      
      // Connect the RTCP
      if ( connection == null ) {
        // println("Spouštím konstruktor RTSP() je spouštěn");
        connection = new RTSP( main );
      }
      
      // Connect the DMX
      if ( dmx != null ) {
        dmx = new DmxP512( main, universeSize, false );
        dmx.setupDmxPro( DMXPRO_PORT, DMXPRO_BAUDRATE );
      }
      
      // set the connectivity state
      this.connected = true;
      
      // nastavit periferie
      this.mountPeriferies();
      
    }
    
  }
  
  /* Unconnect devices */
  public void unconnect(){

    this.connected = false;
    
    // unconnect the DMX
    dmx = null;
    
    // unconnect the IP camera
    connection = null;
    
    // unmount periferies
    periferies = new Output[0];
  }
  
  /* Mount periferies */
  public void mountPeriferies() {
  
    // The Outputs config
    
    // println( "mountuji periferie ////////////////////////////////////////////////////////////" );
    
    JSONArray outs = c.getOutputs();
    
    periferies = new Output[ outs.size() ];
    
    // Array of the periferies
      
    if ( outs.size() > 0 ) {
    
      for ( int i=0; i<outs.size();i++ ) {
        JSONObject output = outs.getJSONObject(i);
        
        switch ( output.getString("type") ){
        
          case "display":
            periferies[i] = new DisplayOutput( output );
            break;
          case "dmx":
            periferies[i] = new DMXOutput( output );
            break;
          case "dmxHead":
            periferies[i] = new DMXHeadOutput( output );
            break;
          case "sound":
            periferies[i] = new SoundOutput( output );
            break;
          case "synth":
            periferies[i] = new SynthOutput( output );
            break;
          case "song":
            periferies[i] = new SongOutput( output );
            break;
          case "debug":
            periferies[i] = new DebugOutput( output );
            break;
        
        }
      }
      
    }
    
    // println("Celkem jsem namountoval " + periferies.length + " periferií.");
  
  }
}
abstract class Output {

  JSONObject setting;
  
  Output( JSONObject obj ){
    this.setting = obj;
  }
  
  public void update(){
    
    this.retrieve();
    this.process();
    
  }
  
  abstract public void retrieve();
  
  abstract public void process();

}

class DisplayOutput extends Output {
  
  private PImage scale;
  private int data[];
  private PImage converted;

  DisplayOutput( JSONObject obj ){
    super(obj);
    this.scale = loadImage( obj.getString( "scale" ) );
  }
  
  public void retrieve(){
    
    // Assign data to the local buffer
    this.data = connection.getLightData();
    
    // Convert data to the image buffer
    this.mapDataToImage();
    
  }
  
  public void process(){
    if (c.debug()) {
      image(this.converted,this.setting.getInt("debugX"),this.setting.getInt("debugY"));
    }  
  }
  
  private void mapDataToImage(){
  
    this.converted = createImage( c.width(), c.height(), RGB );
    
    this.converted.loadPixels();
    
    for ( int i=0;i<this.data.length;i++ ) {
      
      this.converted.pixels[i] = this.scale.pixels[ this.data[i] ];
      
    }
    
    this.converted.updatePixels();
  
  }
  
}

class DMXOutput extends Output {
  
  PImage scale;
  int value;
  DMXFixture fixture;

  DMXOutput( JSONObject obj ){
    super(obj);
    this.scale = loadImage( setting.getString("scale") );
    this.value = 0;
    // println( this.setting.getJSONObject("fixture") );
    this.fixture = new DMXFixture( this.setting.getJSONObject("fixture") );
  }
  
  public void retrieve(){
    this.value = (int) constrain( map(connection.getLightFocalInt(), c.lightMin(), c.lightMax(),0,255), 0,255 );
  }
  
  private int getMappedColor(){
    return this.scale.pixels[constrain(this.value,0,255)];
  }
  
  public void process(){
    
    this.fixture.sendColor( this.getMappedColor() );
  
  
    // Debug output
    if ( c.debug() ) {
    
      pushMatrix(); push();
      
      translate( this.setting.getInt("debugX"), this.setting.getInt("debugY") );
      
      noStroke();
      fill( this.getMappedColor() );
      
      rect(0,0,100,100);
      
      popMatrix(); pop();
      
    }
    
  }
  
}

class DMXHeadOutput extends DMXOutput {
  

  DMXHeadOutput( JSONObject obj ){
    super(obj);
  }
  
  @Override
  public void retrieve(){
    
    float totalTemp = connection.getFocal();
    
    if ( totalTemp < this.setting.getFloat("min") ) { 
      this.value = 0;
    } else {
      this.value = (int) map( totalTemp, this.setting.getFloat("min"), this.setting.getFloat("max"),0,255 );
    }
    
  }
  
  public void process(){
    super.process();
  }
  
}



class SoundOutput extends Output {
  
  float value, focus;
  
  float triggerMin, triggerMax;
  int triggerCount, triggerLimit;
  float[] triggerBuffer;
  
  boolean soundIsPlaying;
  
  float soundFocusedMin, soundFocusedMax;
  float soundDuration, soundLife;

  SoundOutput( JSONObject obj ){
    super(obj);
    
    // Current Value
    this.value = 0;
    this.focus = 0;
    
    // Trigger
    this.triggerMin = this.setting.getFloat("min");
    this.triggerMax = this.setting.getFloat("max");
    this.triggerCount = 0;
    this.triggerLimit = this.setting.getInt("limit");
    this.triggerBuffer = new float[this.setting.getInt("bufferLength")];
    for (float f : this.triggerBuffer) { f = 0; }
    
    // The song
    this.soundIsPlaying = false;
    this.soundFocusedMin = this.setting.getFloat("focusMin");
    this.soundFocusedMax = this.setting.getFloat("focusMax");
    this.soundDuration = this.setting.getInt("duration");;
    this.soundLife = 0;
     
  }
  
  public void retrieve(){
    
    this.passNext();
    
    if ( !this.soundIsPlaying ) {
      
      // Eliminate the pause
      if ( this.triggerCount < 0 ) {
        this.triggerCount++;
      }
      
      // Adjust the amplitude
      if ( this.focus != 0 ) { this.focus = 0; }
      
      // Grow the buffer or reset it
      if ( this.bufferIsOk() ) {
        this.triggerCount++;
      } else {
        this.triggerCount = 0;
      }
      
      // Process the trigger
      if ( this.triggerCount >= this.triggerLimit ) {
        this.soundIsPlaying = true;
        this.triggerCount = 0 - this.setting.getInt("pause");
      }
      
    } else {
      
      // Adjust the focus aspect
      this.mapFocus();
    
      // Play the song if not ended yet
      if ( this.soundLife < this.soundDuration ) {
        
        this.liveTheLifeOfTheSound();
      
      }
      // end the song if already ended
      else {
        this.soundIsPlaying = false;
        this.soundLife = 0;
      }
    
    }
    
    
  }
  
  public void mapFocus(){
    float focusMap = map( this.value, this.triggerMin, this.triggerMax,this.soundFocusedMin, this.soundFocusedMax);
    float focusLerp = lerp( focusMap, this.focus, 0.9f );
    this.focus = constrain( focusLerp, this.soundFocusedMin, this.soundFocusedMax );
  }
  
  public void liveTheLifeOfTheSound(){
    // this.soundLife++;
  }
  
  public void passNext(){
    
    this.value = connection.getFocal();
  
    for ( int i=0; i<this.triggerBuffer.length - 1;i++ ) {
      this.triggerBuffer[i+1] = this.triggerBuffer[i];
    }
    
    this.triggerBuffer[0] = this.value;
  
  }
  
  public boolean bufferIsOk(){
    boolean output = false;
    
    for ( float f : this.triggerBuffer ) {
      if ( f >= this.triggerMin && f <= this.triggerMax ) {
        output = true;
      }
    }
    
    return output;
  }
  
  public void process(){
  
    
    if ( c.debug() ) {
    
      pushMatrix(); push();
      
      translate(this.setting.getInt("debugX"),this.setting.getInt("debugY"));

    
      // The trigger
      float mappedCount = map( this.triggerCount, 0, this.triggerLimit, 0, 100 );
      
      noStroke();
      fill(0,255,255);
      text("Spouštěč zvuku: " + this.triggerCount, 0,20);
      
      rect(0, 30, mappedCount, 10);
      
      noFill();
      strokeWeight(1);
      stroke(0,255,255);
      
      rect(0, 30, mappedCount, 10);
      
      // The focus
      float mappedFocus = map( this.focus, 0, this.soundFocusedMax, 0, 100 );
      
      noStroke();
      fill(0,255,255);
      text("Focus zvuku: " + this.focus, 0,60);
      
      rect(0, 80, mappedFocus, 10);
      
      noFill();
      strokeWeight(1);
      stroke(0,255,255);
      
      rect(0, 80, mappedFocus, 10);
      
      // The duration
      float mappedDuration= map( this.soundLife, this.soundDuration, 0, 0, 100 );
      
      noStroke();
      fill(0,255,255);
      text("Trvání zvuku: " + this.soundLife + " z " + this.soundDuration, 0,100);
      
      rect(0, 120, mappedDuration, 10);
      
      noFill();
      strokeWeight(1);
      stroke(0,255,255);
      
      rect(0, 120, mappedDuration, 10);
      
      
      popMatrix(); pop();
    
    }
    
    
  }
  
}

class SongOutput extends SoundOutput {
  
  Sound sound;
  int numSamples;

  SongOutput( JSONObject obj ){
    super(obj);
    this.sound = new MusicalSound();
  }
  
  @Override
  public void mapFocus(){
    this.focus = map(this.value, this.triggerMin, this.triggerMax,this.soundFocusedMin, this.soundFocusedMax);
    this.focus = constrain( this.focus, this.soundFocusedMin, this.soundFocusedMax );
  }
  
  @Override
  public void liveTheLifeOfTheSound(){
    this.soundLife++;
  }
  
  public void process(){
    super.process();
    this.sound.update( this );
  }
  
  
}

class SynthOutput extends SoundOutput {
  
  Sound sound;

  SynthOutput( JSONObject obj ){
    super(obj);
    this.sound = new HeartSound();
  }
  
  @Override
  public void liveTheLifeOfTheSound(){

    if (this.bufferIsOk()) {
      this.soundLife = 0;
    } else {
      this.soundLife++;
    }

  }
  
  public void retrieve(){
    super.retrieve();
  }
  
  public void process(){
    super.process();
    this.sound.update( this );
  }
  
}

class DebugOutput extends Output {

  DebugValue infos[];
  
  DebugOutput( JSONObject obj ){
    super(obj);
    
    this.infos = new DebugValue[6];
    this.infos[0] = new DebugValue( "Světlo: centrum", color(255), "focalLight", c.lightMin(), c.lightMax() );
    this.infos[1] = new DebugValue( "Světlo: minimuim", color(255), "minLight", c.lightMin(), c.lightMax() );
    this.infos[2] = new DebugValue( "Světlo: maximum", color(255), "maxĹight", c.lightMin(), c.lightMax() );
    this.infos[3] = new DebugValue( "Totální centrum", color(0,255,255), "focal", c.totalMin(), c.totalMax() );
    this.infos[4] = new DebugValue( "Totální průměr", color(0,255,255), "average", c.totalMin(), c.totalMax() );
    this.infos[5] = new DebugValue( "Pan", color(255,0,0), "pan", -1, 1 );


  }
  
  public void retrieve(){}
  
  class DebugValue {
    
    String name, val;
    int col;
    float value, min, max;
    
    DebugValue( String name_, int col_, String val_, int min_, int max_ ){
      this.name = name_;
      this.col = col_;
      this.val = val_;
      this.min = (float) min_;
      this.max = (float) max_;
      this.value = this.min;
    }
    
    DebugValue( String name_, int col_, String val_, float min_, float max_ ) {
      this.name = name_;
      this.col = col_;
      this.val = val_;
      this.min = min_;
      this.max = max_;
      this.value = this.min;
    }
    
    public void pull(){
      switch ( this.val ) {
      
        case "focal":
          this.value = connection.getFocal(); break;
        case "focalLight":
          this.value = connection.getLightFocalFloat(); break;
        case "average":
          this.value = connection.getAverage(); break;
        case "averageLight":
          this.value = connection.getLightAverageFloat(); break;
        case "min":
          this.value = connection.getMin(); break;
        case "minLight":
          this.value = connection.getLightMinFloat(); break;
        case "max":
          this.value = connection.getMax(); break;
        case "maxĹight":
          this.value = connection.getLightMaxInt(); break;
        case "pan":
          this.value = connection.getPan(); break;
        
      }
    }
    
    public void render() {
      
      this.pull();
      
      push();
      
      noStroke();
      fill( this.col );
      
      text( this.name + ": " + this.value + " (" + this.min + " - " + this.max + ")", 0,20 );
      rect( 0, 20, map(this.value, this.min, this.max, 0,255), 10 );
      
      noFill();
      stroke( this.col );
      rect( 0, 20, map(this.value, this.min, this.max, 0,255), 10 );
       
      pop();
    }
  
  }
  
  
  public void process(){
  
    pushMatrix(); push();
    
    translate(this.setting.getInt("debugX"),this.setting.getInt("debugY"));
    
    int i=0; int h = 30;
    for ( DebugValue v : this.infos ) {
      
      pushMatrix();
    
      translate(0,i*h);
      
      v.render();
      i++;
    
      popMatrix();
      
    }
    
    popMatrix(); pop();
  }
  
}
class RTSP {

  Client cli;
  private boolean streaming, pingScheduled;
  private int streamingTime, streamingInterval, streamingResolution;
  // private float streamigResolution;
  private String session;
  
  private ArrayList<RTSPState> states;
  private int listeningForState;
  private int listeningForCSeq;
  
  // Incoming stream
  private boolean rtpReadsData;
  private int rtpReadCount;
  private byte[] rtpBuffer;
  
  // New incoming stream
  private int rtpFrameRead;
  private byte[] rtpFrameBuffer;
  private boolean rtpFrameHasStarted;
  private int frameDuration, frameDurationLimit;
  
  // The result output
  // private int[] mappedBuffer;
  // private int average, min, max, focal;
  
  // The light output
  private int[] lightBuffer;
  
  // The total output
  private float[] totalBuffer;
  private float totalMin, totalMax, totalAverage, totalFocal, totalPan;
  
  RTSP( PApplet main_ ){
    
    println("RTSP konstruktor uvnitř");
  
    // RTSP connection
    this.cli = new Client(main_, c.ip(), c.port());
    
    // RTSP setup
    this.streaming = false;
    this.pingScheduled = false;
    this.listeningForState = 0;
    this.listeningForCSeq = 0;
    this.session = null;
    this.streamingTime = 0;
    this.streamingResolution = 30;
    this.streamingInterval = 120;
    
    // Setup of states
    this.states = new ArrayList<RTSPState>();
    
    // Incoming RTP
    this.rtpReadsData = false;
    this.rtpBuffer = new byte[0];
    this.rtpReadCount = 0;
    
    this.frameDuration = 0;
    this.frameDurationLimit = 6*60;
    
    // Mapped settings
    // this.mappedBuffer = new int[0];
    this.lightBuffer = new int[0];
    this.totalBuffer = new float[0];
    
    // Configure the first state
    RTSPState s1 = new RTSPState( 0, c.ip(), c.port(), "OPTIONS" );
    s1.setURI("thermal.sdp");
    
    // Configure the second state
    RTSPState s2 = new RTSPState( 1, c.ip(), c.port(), "DESCRIBE" );
    s2.setURI("thermal.sdp");
    
    // Configure the third state
    RTSPState s3 = new RTSPState( 2, c.ip(), c.port(), "SETUP" );
    s3.setURI("thermal.sdp");
    s3.setLine("Transport", "RTP/AVP/TCP;unicast;interleaved=0-1");
    
    // Configure the fourth state
    RTSPState s4 = new RTSPState( 3, c.ip(), c.port(), "PLAY" );
    s4.setURI("thermal.sdp");
    s4.setLine("Session", "RTP/AVP/TCP;unicast;interleaved=0-1");
    
    // The ping
    RTSPState s5 = new RTSPState( 4, c.ip(), c.port(), "OPTIONS" );
    s5.setURI("thermal.sdp");
    s5.setLine("Session", this.session );
    
    // Set parameter
    RTSPState s6 = new RTSPState( 5, c.ip(), c.port(), "SET_PARAMETER" );
    s6.setURI("thermal.sdp");
    s6.setLine("Content-Type", "text/parameters" );
    int contentLength = ("interface.palette.scale.min:" + c.lightMin() + "\ninterface.palette.scale.max" + c.lightMax() + "\n").length();
    s6.setLine( "Content-Length", str(contentLength) );
    s6.setLine( "interface.palette.scale.min", str( c.lightMin() ) );
    s6.setLine( "interface.palette.scale.max", str( c.lightMax() ) );

    
    
    // Append all states to the ArrayList
    this.states.add( s1 );
    this.states.add( s2 );
    this.states.add( s3 );
    this.states.add( s4 );
    this.states.add( s5 );
    this.states.add( s6 );
    
    this.cli.write( s1.getMessage() );
    
  }
  
  /* Routines */
  
  /* Updates each turn */
  public void update() {
    
    if ( this.streaming ) {
      
      int pingFreq = 30 * 60;
      
      if ( this.streamingTime >= pingFreq ) {
        
          this.cli.write( this.states.get(4).getMessage() );
          
          this.states.get(4).increaseCseq();
          
          this.streamingTime = 0;
      
      } else {
        
        this.streamingTime++;

      }
      
      // Detekuje delay mezi jednotlivými framy
      // Timeout checking
      if ( this.streaming ) {
        this.frameDuration++;
      }
      
      if ( this.frameDuration >= this.frameDurationLimit ) {
          m.unconnect();
          m.setCameraState(false);
        }
      
    }
  
  }
  
  /* Listening to server messages */
  public void listen(){
  
    // Proceed only when the serial has sent any input
    if ( this.cli.available() > 0 ) {
       
      // 1: Streaming setup
      if ( ! this.streaming ) {
        
        // 
        String msg = this.cli.readString();
        int msgCseq = RTCPParser.cseq( msg );
        int msgStatus = RTCPParser.status( msg );
        
        // Continue only on OK messages corresponding to current state and CSeq
        if ( 
          msgStatus == 200
          && msgCseq == this.listeningForCSeq
        ) {
          
          println( "Úspěšně jsem obdržel zprávu:" );
          println(msg);
          println( "Nyní čekám na " + this.listeningForState );
          println( "////////////" );

          // Control if the message corresponds to the current state
          switch ( this.listeningForState ) {
          
            case 0:
              if ( RTCPParser.getStringValueAt("Public", msg).contains("OPTIONS, DESCRIBE, PLAY, PAUSE, SETUP, TEARDOWN") ) {
                // The status 0 is successfull
                this.listeningForState = 1;
                this.listeningForCSeq = this.states.get(1).cseq();
                String message = this.states.get(1).getMessage();
                this.cli.write( message );
              }
              break;
            case 1:
              if ( RTCPParser.getStringValueAt("Content-Type", msg).contains("application/sdp") ) {
                
                this.listeningForState = 2;
                this.listeningForCSeq = this.states.get(2).cseq();
                this.cli.write( this.states.get(2).getMessage() );
              
              }
              break;
            case 2:
              if ( RTCPParser.getStringValueAt("Session", msg) != null ) {
                println(RTCPParser.getStringValueAt("Session", msg));
                
                this.session = RTCPParser.getStringValueAt("Session", msg);
                this.listeningForCSeq = this.states.get(3).cseq();
                this.listeningForState = 3;
                this.states.get(3).setLine("Session", this.session );
                this.states.get(4).setLine("Session", this.session );
                
                this.cli.write( this.states.get(3).getMessage() );
              }
              break;
              /*
            case 3:
            println( this.states.get(5).getMessage() ); 
              if ( RTCPParser.getStringValueAt("Content-Type", msg) != null ) {
                // println(RTCPParser.getStringValueAt("Session", msg));
                
                this.session = RTCPParser.getStringValueAt("Session", msg);
                this.listeningForCSeq = this.states.get(5).cseq();
                this.listeningForState = 5;
                // this.states.get(5).setLine("Session", this.session );
                
                
                
                this.cli.write( this.states.get(5).getMessage() );
              }
              break;
              */
            case 3:
            println( msgCseq );
              if ( msgCseq == 3 ) {
                println("Mohu běžet!");
                this.streaming = true;
              }
              break;
            
          }
        
        } 
        // Debug setup output
        else {
        
          println("Setting up sequence at phase " + this.listeningForState +": GOT ERROR:");
          println(msg);
          if ( RTCPParser.getStringValueAt("Session", msg) != null ) {
            if ( RTCPParser.getStringValueAt( "Public", msg ) != null ) {
              println( "Přijal jsem Ping" );
              this.streaming = true;
              
              // Clear the buffer
              this.cli.clear();
            }
          }
          
        }
        
      } // End setting up sequence
      
      // 2: Streaming itself
      else {        
        
        // Uloží celou zprávu do bufferu
        byte[] message = this.cli.readBytes();
        this.cli.clear();
        
        // pokud začíná nový frame, čti tento frame
        // 
        if ( message[0] == 'R' && message[1] == 'T' && message[2]=='S' ) {
          println("XXXXXXXXXXXX přijímám hlavičku odpovědi na ping //////////////////////////////////////");
        } else if ( message[0] == '$' ) {
          this.rtpFrameHasStarted = true;
          this.rtpFrameRead = 0;
          int size = (int)(message[2] << 8 | message[3]) & 0xFFFF;
          
          // Ofset = 17-18
          int offset = binToInt( message[17], message[18] );
          
          this.rtpFrameBuffer = new byte[ message.length - 19 ];
          
          int bCount = 0;
          for ( int i = 19; i<message.length; i++ ) {
            this.rtpFrameBuffer[bCount] = message[i];
            bCount++;
          }
          
          this.processFrame();
          
        } 
        if ( message[0] != '$' && message[0] != 'R' ) {
          println("ostatní stream");
        }
        
        this.frameDuration = 0;
        
        
        
        
        
        
        
        /*
        
        // Pokud nečtu čtu data, čekám na hlavičku
        // hlavička začíná znakem $
        if ( ! this.rtpReadsData ) {
          
          // načíst první 4 byty, tedy hlavičku
          byte[] header = this.cli.readBytes( 4 ); 
          
          // zkontrolovat, zda se jedná o začátek hlavičky
          if ( header[0] == '$' ) {
            
            
            int size = (int)(header[2] << 8 | header[3]) & 0xFFFF;
            
            // Processovat buffer
            if ( this.rtpBuffer.length > 0 ) {
              this.processBuffer();
            }
            
            
            
            // Reinicializovat buffer
            this.rtpBuffer = new byte[size];
            //// this.rtpReadCount = 0;
            
            // Uložit zbytek data do bufferu
            this.rtpReadCount = this.cli.readBytes(this.rtpBuffer);
            // this.rtpReadCount = header.length - 30;
            
            // Vyčistí buffer do nuly
            this.cli.clear();
            
            // začít číst data
            this.rtpReadsData = true;
            
            
          }
        
        }
        
        // Pokud čtu data, ukládám celou zprávu do bufferu
        else {
          
          
          
          // Vytvořit si prozatimní buffer
          byte[] bufferTmp = this.cli.readBytes( this.rtpBuffer.length - this.rtpReadCount );
          
          // println( bufferTmp[0] );
          
          for ( int i=0; i<bufferTmp.length; i++ ) {
            byte b = bufferTmp[i];
            this.rtpBuffer[ i + this.rtpReadCount ] = b;
          }
          
          this.rtpReadCount += bufferTmp.length;
          
          if ( this.rtpReadCount == this.rtpBuffer.length ) {
          
            this.rtpReadsData = false;
            
            // println( "Teď mi končí frame" );
            if ( this.pingScheduled == true ) {
              
              println(frameCount + " /////////////////////////////////////////");
            
              this.pingScheduled = false;
            
              this.streaming = false;
          
              this.cli.write( this.states.get(4).getMessage() );
          
              this.states.get(4).increaseCseq();
          
              this.listeningForState = 9;
            
            }
            
            
          }
          
          
        }
        
        */
        
        
        // Pokud nečtu data, čekám na hlavičku
        
          // Hlavička je 4 bajty dlouhá
          
            // Pokud 1. bajt == $ >>>>> hlavička začíná
            
            // Zjistit velikost správy a vytvořit prázdný pole o této velikosti to this.buffer
            
            // this.rtcpReadData = 0
            // this.buffer = new byte[size];
            
            // int length = this.cli.readBytes( this.buffer );
            // this.readData += length;
        
        // pokud čtu data, zapisuju do bufferu
        
          // byte[] bufferTmp = this.cli.readBytes( this.buffer.length - this.readData );
          
          // for ( byte b : bufferTmp ) {
            // this.buffer[ this.readData + indexV Rámci Smyčky Začínající od nuly ] = b;
          // }
          
          // this.readData += bufferTmp.length
          
          // if ( this.readData = this.buffer.length )  {this.readsData = true; }        
      
      } // End of streaming loop
    
    }
  
  }
  
  
  /* Setters */
  
  private void processFrame(){
    byte[] buf = new byte[ this.rtpFrameBuffer.length ];
    arrayCopy( this.rtpFrameBuffer, buf );
    // println("Zpracovávám buffer");
    // println( buf.length / 2 );
    // println(  );
    
    // Reinicializovat kelvinový buffer
    int numPixels = (int) ( buf.length / 2 );
    this.totalBuffer = new float[ numPixels ];
    this.lightBuffer = new int[ numPixels ];
    
    // Reinicializace proměnných
    this.totalMin = 9999;
    this.totalMax = -9999;
    this.totalAverage = 0;
    this.totalPan = 0;
    
    // temporary variables
    float totalFocalSummer = 0;
    int totalFocalPxCount = 0;
    float totalAverageSummer = 0;
    
    int pxCount = 0;
    
    for ( int i = 0; i < buf.length; i +=2 ) {
    
      int raw = (( buf[i+1] & 0xFF )<< 8 ) | (buf[i] & 0xFF);
      
      // Unit conversion
      float kelvin = raw/100f;
      float celsius = kelvin - 273.15f;
      
      // trim celsius in total range
      celsius = this.celsiusInTotalRange( celsius );
      
      // Calculate the coordinates of the pixel
      int x = pxCount % c.width();
      int y = (int) pxCount / c.width();
      
      // Calculate the absolute focal value
      if ( x > c.width()/2 - c.centerRadius() && x < c.width()/2 + c.centerRadius() ) {
      
        if ( y > c.height()/2 - c.centerRadius() && y < c.height()/2 + c.centerRadius() ) {
        
          totalFocalSummer += celsius;
          totalFocalPxCount++;
        
        }
      
      }
      
      // Total average
      totalAverageSummer += celsius;
      
      // Total minimum & maximum
      if ( celsius < this.totalMin ) { this.totalMin = celsius; }
      if ( celsius > this.totalMax ) { this.totalMax = celsius; }
      
      // Total buffer assignement
      this.totalBuffer[pxCount] = celsius;
      
      // Light buffer assignment
      this.lightBuffer[pxCount] = (int) map( this.celsiusInLightRangeFloat(celsius), c.lightMin(), c.lightMax(), 0, 255 );
      
      pxCount++; 
    
    }
    
    this.totalAverage = totalAverageSummer / pxCount;
    this.totalFocal = totalFocalSummer / totalFocalPxCount;
    
    // Calculate the pan
    float panTreshold = this.totalAverage; // lerp( this.totalAverage, c.totalMax(), 0.5 );
    int panSummer = 0;
    int panCount = 0;
    for ( int i=0;i<this.totalBuffer.length;i++ ) {
      
      if ( this.totalBuffer[i] > panTreshold ) {
      
        panSummer += i % c.width();
        panCount++;
      
      }
      
    }
    
    if ( panCount == 0 ) {
      this.totalPan = 0;
    } else {
      this.totalPan = map( panSummer/panCount, 0,c.width(), -1, 1 );
    }
    
    
    
  }
  
  /* Process the buffer */
  private void processBuffer(){
  
    byte[] buf = new byte[ this.rtpBuffer.length ];
    arrayCopy( this.rtpBuffer, buf );
    
    // println( "Délka bufferu " + this.rtpBuffer.length );
    
    // Zjistit si offset, který je v bufferu na místě 17-18
    int offset = binToInt( buf[13], buf[14] );
    
    
    // Reinicializovat si buffer
    int numPixels = ( buf.length - 15 ) / 2;
    
    this.totalBuffer = new float[ numPixels ];
    
    // nová reinicializace proměnných
    this.lightBuffer = new int[ numPixels ];
    this.totalBuffer = new float[ numPixels ];
    this.totalMin = 9999;
    this.totalMax = -9999;
    this.totalAverage = 0;
    this.totalPan = 0;
    
    // temporary variables
    float totalFocalSummer = 0;
    int totalFocalPxCount = 0;
    float totalAverageSummer = 0;
    
    
    // PVector center = new PVector( c.width()/2, c.height()/2 );

    // Procesovat buffer
    int pxCount = 0;
    
    for ( int i = 15; i < buf.length; i +=2 ) {
      
      // Convert the bytes to integer value
      int raw = (( buf[i+1] & 0xFF )<< 8 ) | (buf[i] & 0xFF);
      
      // Unit conversion
      float kelvin = raw/100f;
      float celsius = kelvin - 273.15f;
      
      // Trim the celsius in the working range
      celsius = this.celsiusInTotalRange( celsius );
      
      // Calculate the coordinates of the pixel
      int x = pxCount % c.width();
      int y = (int) pxCount / c.width();
      
      // Calculate the absolute focal value
      if ( x > c.width()/2 - c.centerRadius() && x < c.width()/2 + c.centerRadius() ) {
      
        if ( y > c.height()/2 - c.centerRadius() && y < c.height()/2 + c.centerRadius() ) {
        
          totalFocalSummer += celsius;
          totalFocalPxCount++;
        
        }
      
      }
      
      // Total average
      totalAverageSummer += celsius;
      
      // Total minimum & maximum
      if ( celsius < this.totalMin ) { this.totalMin = celsius; }
      if ( celsius > this.totalMax ) { this.totalMax = celsius; }
      
      // Total buffer assignement
      this.totalBuffer[pxCount] = celsius;
      
      // Light buffer assignment
      this.lightBuffer[pxCount] = (int) map( this.celsiusInLightRangeFloat(celsius), c.lightMin(), c.lightMax(), 0, 255 );
      
      pxCount++;
      
    }
    
    //
    
    this.totalAverage = totalAverageSummer / pxCount;
    this.totalFocal = totalFocalSummer / totalFocalPxCount;
    
    // Calculate the pan
    float panTreshold = this.totalAverage; // lerp( this.totalAverage, c.totalMax(), 0.5 );
    int panSummer = 0;
    int panCount = 0;
    for ( int i=0;i<this.totalBuffer.length;i++ ) {
      
      if ( this.totalBuffer[i] > panTreshold ) {
      
        panSummer += i % c.width();
        panCount++;
      
      }
      
    }
    
    if ( panCount == 0 ) {
      this.totalPan = 0;
    } else {
      this.totalPan = map( panSummer/panCount, 0,c.width(), -1, 1 );
    }

  }
  
  /* Converters */
  
  /* Trim celsius in the total range */
  private float celsiusInTotalRange( float input ){
    if ( input < c.totalMin() ) { input = c.totalMin(); }
    if ( input > c.totalMax() ) { input = c.totalMax(); }
    return input;
  }
  
  /* Trim the celsius in light range */
  private int celsiusInLightRangeInt( float input ){
    if ( input < c.lightMin() ) { input = c.lightMin(); }
    if ( input > c.lightMax() ) { input = c.lightMax(); }
    return (int) input;
  }
  
  private float celsiusInLightRangeFloat( float input ){
    if ( input < c.lightMin() ) { input = c.lightMin(); }
    if ( input > c.lightMax() ) { input = c.lightMax(); }
    return input;
  }
  
  /* Getters */
  
  /* Get the entire data */
  public float[] getData(){ return this.totalBuffer; }
  
  public int[] getLightData(){ return this.lightBuffer; }
  
  public float getAverage(){ return constrain( this.totalAverage, c.totalMin(), c.totalMax() ); }
  
  public int getLightAverageInt() { return (int) constrain( this.celsiusInLightRangeInt( this.totalAverage) , c.totalMin(), c.totalMax() ); }
  
  public float getLightAverageFloat() { return constrain( this.celsiusInLightRangeFloat( this.totalAverage ), c.totalMin(), c.totalMax() ); }
  
  public float getMin(){ return constrain( this.totalMin, c.totalMin(), c.totalMax()) ; }
  
  public int getLightMinInt(){ return this.celsiusInLightRangeInt( this.totalMin ); }
  
  public float getLightMinFloat(){ return this.celsiusInLightRangeFloat( this.totalMin ); }
  
  public float getMax(){ return this.totalMax; }
  
  public int getLightMaxInt(){ return this.celsiusInLightRangeInt( this.totalMax ); }
  
  public float getLightMaxFloat(){ return this.celsiusInLightRangeFloat( this.totalMax ); }
  
  public float getFocal(){ return constrain( this.totalFocal,c.totalMin(), c.totalMax()); }
  
  public int getLightFocalInt() { return (int) constrain( this.celsiusInLightRangeInt( this.totalFocal ), c.lightMin(), c.lightMax() ); }
  
  public float getLightFocalFloat() { return constrain( this.celsiusInLightRangeFloat( this.totalFocal ), c.lightMin(), c.lightMax() ); }
  
  public float getPan(){ return this.totalPan; }
  
}


public int binToInt( byte left, byte right ){
  return (int) (left << 8 | right) & 0xFFFF;
}

public short binToShort( byte left, byte right ){
  return (short) ( (right << 8 | left) & 0xFFFF );
}
class RTSPState {
  
  private int cseq;
  private boolean processed;
  
  // Connection details 
  private String serverIP;
  private int serverPort;
  private String serverURI;
  
  // Message content
  private String header;
  private StringDict msg;
  private String message;

  RTSPState( int cseq_, String ip_, int port_, String header_ ){
  
    // State attributes
    this.cseq = cseq_;
    this.processed = false;
    
    // Connection details
    this.serverIP = ip_;
    this.serverPort = port_;
    
    // Message content
    this.header = header_;
    this.msg = new StringDict();
    
  }
  
  // Helpers
  
  // Render the message
  public void renderMessage(){
  
    // First line - header info
    this.message = this.header + " rtsp://" + this.serverIP + ":" + this.serverPort;
    
    // If an URI is present, add that
    if ( this.serverURI != null ) {
      this.message += "/"+this.serverURI;
    }
    
    this.message += " RTSP/1.0";
    
    // Second line - CSeq
    this.message += "\nCSeq: " + this.cseq + "\n";
    
    
    // Third line and more - optional parameters
    if ( this.msg.size()>0 ) {
    
      for ( String k : this.msg.keyArray() ) {
        
        this.message += k + ": " + this.msg.get(k) + "\n";
        
      }
    
    }
  
  }
  
  // Getters
  public boolean isProcessed(){
    return this.processed;
  }
  public int cseq(){
    return this.cseq;
  }
  public String getMessage() {
    this.renderMessage();
    return this.message;
  }
  
  // Setters
  
  // Set the processed state
  public void process(){
    this.processed = true;
  }
  
  // Set the source uri
  public void setURI( String uri ){
    this.serverURI = uri;
  }
  
  // Add an option to the message
  public void setLine(String name, String value){
    this.msg.set(name,value);
  }
  
  public void setCseq( int i ){
    this.cseq = i;
  }
  public void increaseCseq(){
    this.cseq++;
  }
  
}

static class RTCPParser {

  RTCPParser(){}
  
  /* Parse the message Cseq */
  public static int cseq( String msg ){
  
    int o = -2;
    
    msg = msg.substring( msg.indexOf("CSeq: ") + 1 );
    msg = msg.substring( 5, msg.indexOf("\n") - 1 );
    
    o = Integer.parseInt(msg);
    
    return o;
  }
  
  public static int status( String msg ){
    
    String rows[] = msg.split("\n");
    
    int status = 0;
    
    if ( rows.length > 0 ) {
    
      String words[] = rows[0].split(" ");
      status = Integer.parseInt( words[1] );
    
    }
    
    return status;
    
  }
  
  public String[] splitByRows( String msg ){
    return msg.split("\n");
  }
  
  public static String getStringValueAt( String attribute, String msg ){
  
    String output = null;
    
    String rows[] = msg.split("\n");
    
    for ( String row : rows ) {
      if ( row.contains(attribute) ) {
        output = row.substring( attribute.length() + 2 );
      }
    }
    
    return output;
  
  }

}


abstract class Sound {
  
  Instrument inst;

  Sound(){}
  
  abstract public void start();
  
  abstract public void update( SoundOutput controller );
  
  abstract public void end();

}

class Song extends Sound {

  Song(){
    super();
  }
  
  public void start(){}
  
  public void update( SoundOutput controller ){
  
  }

  public void end(){}
  
}

class FMSound extends Sound {
  
  Oscil osc, modulator;
  Balance bal;
  float freq, cf;
  float amp, ampMax;

  FMSound(){
    super();
    this.cf = 80;
    this.freq = this.cf;
    this.amp = 0.0f;
    this.ampMax = 0.5f;
    this.start();
  }
  
  public void start(){
    this.osc = new Oscil( this.freq, this.amp, Waves.SINE );
    this.modulator = new Oscil( 10, 2, Waves.SINE );
    this.modulator.offset.setLastValue(this.freq);
    this.modulator.patch( this.osc.frequency );
    
    this.bal = new Balance(0f);
    this.osc.patch( dac );
  }
  
  public void update( SoundOutput controller ){
    
    // Process the amplitude;
    if ( controller.soundIsPlaying && this.amp < this.ampMax ) {
      this.amp += 0.05f;
    }
    if ( ! controller.soundIsPlaying && this.amp > 0) {
      this.amp -= 0.01f;
    }
    this.osc.setAmplitude( this.amp );
    
    // FM
    // if ( frameCount % 1 == 0) {
      this.freq = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 0.5f,100f );
      this.modulator.setFrequency( this.freq  );
      
      float modulateAmp = map( connection.getLightAverageFloat(), c.lightMin(),c.lightMax(), this.cf, 1 );
      this.modulator.setAmplitude( modulateAmp * this.amp );
      
      // float amplitude = map( controller.focus, 0,1,0,0.5);
      // this.osc.setAmplitude( this.amp );
      // dac.setGain( amplitude );
      
      // float bla = constrain( connection.getPan(), -1f, 1f  );
      // this.bal.setBalance( bla );
    // }
    
  }

  public void end(){}
  
}

class BlipSound extends Sound {
  
  Oscil osc;
  BandPass bpf;
  Balance bal;
  float freq;
  float amp, maxAmp;

  BlipSound(){
    super();
    this.freq = 3;
    this.amp = 0f;
    this.maxAmp = 1f;
    this.start();
  }
  
  public void start(){
    this.osc = new Oscil( this.freq, this.amp, Waves.SAW );
    this.bpf = new BandPass( 100, 20, dac.sampleRate() );
    this.bal = new Balance( 0 );
    this.osc.patch( this.bpf ).patch( this.bal ).patch( dac );
  }
  
  public void update( SoundOutput controller ){

    // Process the amplitude;
    if ( controller.soundIsPlaying && this.amp < this.maxAmp ) {
      this.amp += 0.05f;
    }
    if ( ! controller.soundIsPlaying && this.amp > 0) {
      this.amp -= 0.01f;
    }
    this.osc.setAmplitude( this.amp );
    
    // BPF & SAWTOOTH
    // if ( frameCount % 1 == 0) {
      
      // Update the frequency based on light focal range
      this.freq = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 1,20f );
      this.osc.setFrequency( this.freq  );
      
      // Update the BPF based on light average value
      float average = (float) map( connection.getLightAverageFloat(), c.lightMin(), c.lightMax(), 20f, 100f );
      this.bpf.setBandWidth( average );
      
      // Update the pan based on the pan
      float bla = constrain( connection.getPan(), -1f, 1f  );
      this.bal.setBalance( bla );
    // }
    
  }

  public void end(){}
  
}

class InstrumentSound extends Sound {
  
  int triggerCounter;
  int triggerLimit;
  int freqMin, freqMax, nextFreq, freqCounter;

  InstrumentSound() {
    super();
    
    this.triggerCounter = 0;
    this.triggerLimit = 5;
    
    this.freqMin = 30; 
    this.freqMax = 180; 
    
    this.generateNextStep();
    
    this.freqCounter = 0;
    
    this.start();
    
  }
  
  private void generateNextStep() {
    this.nextFreq = (int) random( this.freqMin, this.freqMax );
  }
  
  @Override
  public void start(){}
  
  @Override 
  public void end() {}
  
  @Override
  public void update( SoundOutput controller ) {
  
    if ( !isSinging ) {
    
      if ( connection.getFocal() > c.songMin() ) {
      
        this.triggerCounter++;
      
      } else {
        this.triggerCounter = 0;
      }
      
      if ( this.triggerCounter > this.triggerLimit ) {
      
        isSinging = true;
        this.triggerCounter = 0;
        
        this.playSequence();
        
      }
      
    } else {
    
      // if ( this.freqCounter > this.nextFreq ) {
      if ( this.freqCounter > 240 ) {
      
        this.generateNextStep();
        this.freqCounter = 0;
        
        this.playSequence();
        
        
      } else {
        
        this.freqCounter++;
        
      }
      
    }
    
    //  Check if there is a hogher point in the radius otherwise delete it
    if ( connection.getMax() < c.songMin() ) {
      isSinging = false;
    }
  
  }
  
  private void playSequence(){
    
    int dice = (int) random(0,3);
    int baseSeqNum = (int) map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 1, pam.buffers.length-2 );
    
    float pause = 0.5f;
    
    int seq[] = new int[4];
    
    switch ( dice ) {
    
      case 0:
        seq = new int[4];
        seq[0] = 0;
        seq[1] = -1;
        seq[2] = -1;
        seq[3] = 0;
        break;
        
      case 1:
        seq = new int[4];
        seq[0] = 0;
        seq[1] = 1;
        seq[2] = 0;
        seq[3] = -1;
        break;
        
      case 3:
        seq = new int[4];
        seq[0] = 0;
        seq[1] = -1;
        seq[2] = 0;
        seq[3] = 1;
        break;
        
    }
    
    dac.playNote( 0,    1,  new SampleInstrument( baseSeqNum + seq[0], pam ));
    dac.playNote( 0.75f, 1,  new SampleInstrument( baseSeqNum + seq[1], pam ));
    dac.playNote( 1,    1,  new SampleInstrument( baseSeqNum + seq[2], pam ));
    dac.playNote( 1.5f,  1,  new SampleInstrument( baseSeqNum + seq[3], pam ));
  
  }
  
}

class MusicalSound extends Sound {
  
  int triggerCounter;
  int triggerLimit;
  int freqMin, freqMax, nextFreq, freqCounter, chantCounter, chantLimit;
  PImage scale;
  int col;
  int intensity;

  MusicalSound() {
    super();
    
    this.triggerCounter = 0;
    this.triggerLimit = 5;
    
    this.freqMin = 360; 
    this.freqMax = 600; 
    this.chantCounter = 0;
    
    this.generateNextStep();
    
    this.freqCounter = 0;
    this.chantLimit = 3;
    
    this.intensity = 0;
    
    this.col = color(0);
    
    this.scale = loadImage( "head.jpg" );
    
    this.start();
    
  }
  
  private void generateNextStep() {
    this.nextFreq = (int) random( this.freqMin, this.freqMax );
  }
  
  @Override
  public void start(){}
  
  @Override 
  public void end() {}
  
  @Override
  public void update( SoundOutput controller ) {
    
    
    if ( isSinging ) {
        
        this.col = lerpColor( this.col, color(255,148,0), 0.33f );
        
    }
    
    if ( !isSinging ) {
        
        this.col = lerpColor( this.col, color(0), 0.33f );
        
    }
    
    if ( c.debug() ) {
      
      pushMatrix(); push();
      
      translate(300,height/2);
      
      fill(this.col);
      rect(0, 0, 100, 100);
      
      popMatrix(); pop();
    
    }
  
    if ( !isSinging ) {
    
      if ( connection.getFocal() > c.songMin() ) {
      
        this.triggerCounter++;
      
      } else {
        this.triggerCounter = 0;
      }
      
      if ( this.triggerCounter > this.triggerLimit ) {
      
        isSinging = true;
        this.triggerCounter = 0;
        
        this.playSong();
        
      }
      
    } else {
    
      if ( this.freqCounter > this.nextFreq ) {
      // if ( this.freqCounter > 240 ) {
      
        this.generateNextStep();
        this.freqCounter = 0;
        this.chantCounter = 0;
        
        this.playSong();
        
        
      } else {
        
        this.freqCounter++;
        
      }
      
    }
    
    //  Check if there is a hogher point in the radius otherwise delete it
    if ( connection.getMax() < c.songMin() || this.chantCounter > this.chantLimit ) {
      isSinging = false;
    }
  
  }
  
  private void playSong(){
    
    this.chantCounter++;
    
    int dice = (int) random(0,songs.buffers.length);
    
    dac.playNote( 0,    this.getAmplitude(),  new SampleInstrument( dice, songs ));
    // println( numChantsSinging );
  
  }
  
  public float getAmplitude() {
  
    float output = 0.0f;
    
    switch ( this.chantCounter ) {
    
      case 0:
        output = -12f; break;
      case 1:
        output = -30f; break;
      case 3:
        output = -6f; break;
      case 4:
        output = -12f; break;
      case 5:
        output = -18f; break;
      case 6:
        output = -30f; break;
      case 7:
        output = -20f; break;
      default:
        output = -12f; break;
    
    }
    
    return output;
    
  }
  
}

class SampleSound extends Sound {

  Sampler samples[];
  float amplitude;
  float frequency;
  int frequencyMin, frequencyMax, frequencyCounter, index;
  int overallTime, overallLimit, trigger, triggerLimit;
  boolean isPlaying;
  
  SampleSound(){
  
    super();
    
    this.samples = new Sampler[8];
    
    this.frequencyMin = 300;
    this.frequencyMax = 30;
    this.frequencyCounter = 0;
    
    this.overallTime = 0;
    this.overallLimit = 400;
    
    this.isPlaying = false;
    this.trigger = 0;
    this.triggerLimit = 3;
    
    this.start();
    
  }
  
  public void start(){
  
    for ( int i = 1; i < 8; i++ ) {
      
      String fileName = "data/pim/0"+i+".mp3";
      this.samples[i] = new Sampler( fileName, 1, minim );
      
      this.samples[i].patch( dac );
  
    }
    
  }
  
  public void update( SoundOutput controller ){
    
    // check if the maximum is over headLimit
    if ( !this.isPlaying && connection.getFocal() > 43f ) {
      if ( this.trigger < this.triggerLimit ) {
        this.trigger++;
      } else {
        this.isPlaying = true;
        this.trigger=0;
        isSinging = true;
      }
    }
    
    if ( this.isPlaying ) {
    
      this.overallTime++;
      
      if ( this.overallTime < this.overallLimit ) {
        
        // Do the work now
        this.frequency = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), this.frequencyMax, this.frequencyMin );
        this.frequency = 60;
        
        if ( this.frequencyCounter < this.frequency ) {
          
          this.frequencyCounter++;
        
        } else {
          
          this.index = (int) map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 0, 7 );
          
          this.samples[index].trigger();
          
          this.amplitude = map( connection.getFocal(), c.totalMin(), c.totalMax(), 0, 1 );
        
          this.frequencyCounter = 0;
          
        }
      
      } else {
        this.isPlaying = false;
        isSinging = true;
      }
    
    }
  
  }
  
  public void end(){}
  
}


class HeartSound extends Sound {
  
  FilePlayer player;
  BandPass bpf;
  Balance bal;
  Gain gain;
  float freq;
  float amp, maxAmp;
  int life, next;

  HeartSound(){
    super();
    this.freq = 3;
    this.amp = 0f;
    this.maxAmp = 2f;
    this.life = 0;
    this.next = 60;
    this.start();
  }
  
  public void start(){
    this.player = new FilePlayer( minim.loadFileStream("heart_clear.wav") );
    this.bal = new Balance( 0 );
    this.gain = new Gain( 0f );
    this.bpf = new BandPass( 200, 40, dac.sampleRate() );
    
    this.player.play();
    
    this.player.patch( this.bpf ).patch( this.bal ).patch( this.gain ).patch( dac );
  }
  
  public void update( SoundOutput controller ){
    
    this.life++;
    
    if ( this.life >= this.next ) {
      if ( heartAspect > 0.1f ) {
        
        this.player.rewind();
        this.player.play();
        this.life = 0;
        
      }
      
    }
    
    if (isSinging && heartAspect > 0.1f) {
    
      heartAspect -= 0.05f;
    
    }
    
    if ( !isSinging && heartAspect < 1 ) {
      
      heartAspect += 0.05f;
      
    }
    
    this.next = (int) map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 150, 40 );

    // Process the amplitude;
    if ( controller.soundIsPlaying && this.amp < this.maxAmp ) {
      this.amp += 0.05f;
    }
    
    if ( ! controller.soundIsPlaying && this.amp > 0) {
      this.amp -= 0.01f;
    }
    
    this.amp = this.amp > 1 ? 1 : this.amp; 
    this.amp = this.amp < 0 ? 0 : this.amp;
    
    this.gain.setValue( map( this.amp,0,1,-60,0) * heartAspect );
      
      // Update the frequency based on light focal range
      this.freq = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 0.5f,5f );
      
      // Update the BPF based on light average value
      float average = (float) map( connection.getLightAverageFloat(), c.lightMin(), c.lightMax(), 30f, 100f );
      this.bpf.setBandWidth( average );
      
      float bpfFreq = map( connection.getLightAverageFloat(), c.lightMin(), c.lightMax(), 80, 200 );
      
      this.bpf.setFreq( bpfFreq );
      
      // Update the pan based on the pan
      float bla = constrain( connection.getPan(), -1f, 1f  );
      this.bal.setBalance( bla );
    
  }

  public void end(){}
  
}
  public void settings() {  size(600,600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "rtcpcontrol" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
