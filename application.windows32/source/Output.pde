abstract class Output {

  JSONObject setting;
  
  Output( JSONObject obj ){
    this.setting = obj;
  }
  
  public void update(){
    
    this.retrieve();
    this.process();
    
  }
  
  abstract public void retrieve();
  
  abstract public void process();

}

class DisplayOutput extends Output {
  
  private PImage scale;
  private int data[];
  private PImage converted;

  DisplayOutput( JSONObject obj ){
    super(obj);
    this.scale = loadImage( obj.getString( "scale" ) );
  }
  
  public void retrieve(){
    
    // Assign data to the local buffer
    this.data = connection.getLightData();
    
    // Convert data to the image buffer
    this.mapDataToImage();
    
  }
  
  public void process(){
    if (c.debug()) {
      image(this.converted,this.setting.getInt("debugX"),this.setting.getInt("debugY"));
    }  
  }
  
  private void mapDataToImage(){
  
    this.converted = createImage( c.width(), c.height(), RGB );
    
    this.converted.loadPixels();
    
    for ( int i=0;i<this.data.length;i++ ) {
      
      this.converted.pixels[i] = this.scale.pixels[ this.data[i] ];
      
    }
    
    this.converted.updatePixels();
  
  }
  
}

class DMXOutput extends Output {
  
  PImage scale;
  int value;
  DMXFixture fixture;

  DMXOutput( JSONObject obj ){
    super(obj);
    this.scale = loadImage( setting.getString("scale") );
    this.value = 0;
    // println( this.setting.getJSONObject("fixture") );
    this.fixture = new DMXFixture( this.setting.getJSONObject("fixture") );
  }
  
  public void retrieve(){
    this.value = (int) constrain( map(connection.getLightFocalInt(), c.lightMin(), c.lightMax(),0,255), 0,255 );
  }
  
  private color getMappedColor(){
    return this.scale.pixels[constrain(this.value,0,255)];
  }
  
  public void process(){
    
    this.fixture.sendColor( this.getMappedColor() );
  
  
    // Debug output
    if ( c.debug() ) {
    
      pushMatrix(); push();
      
      translate( this.setting.getInt("debugX"), this.setting.getInt("debugY") );
      
      noStroke();
      fill( this.getMappedColor() );
      
      rect(0,0,100,100);
      
      popMatrix(); pop();
      
    }
    
  }
  
}

class DMXHeadOutput extends DMXOutput {
  

  DMXHeadOutput( JSONObject obj ){
    super(obj);
  }
  
  @Override
  public void retrieve(){
    
    float totalTemp = connection.getFocal();
    
    if ( totalTemp < this.setting.getFloat("min") ) { 
      this.value = 0;
    } else {
      this.value = (int) map( totalTemp, this.setting.getFloat("min"), this.setting.getFloat("max"),0,255 );
    }
    
  }
  
  public void process(){
    super.process();
  }
  
}



class SoundOutput extends Output {
  
  float value, focus;
  
  float triggerMin, triggerMax;
  int triggerCount, triggerLimit;
  float[] triggerBuffer;
  
  boolean soundIsPlaying;
  
  float soundFocusedMin, soundFocusedMax;
  float soundDuration, soundLife;

  SoundOutput( JSONObject obj ){
    super(obj);
    
    // Current Value
    this.value = 0;
    this.focus = 0;
    
    // Trigger
    this.triggerMin = this.setting.getFloat("min");
    this.triggerMax = this.setting.getFloat("max");
    this.triggerCount = 0;
    this.triggerLimit = this.setting.getInt("limit");
    this.triggerBuffer = new float[this.setting.getInt("bufferLength")];
    for (float f : this.triggerBuffer) { f = 0; }
    
    // The song
    this.soundIsPlaying = false;
    this.soundFocusedMin = this.setting.getFloat("focusMin");
    this.soundFocusedMax = this.setting.getFloat("focusMax");
    this.soundDuration = this.setting.getInt("duration");;
    this.soundLife = 0;
     
  }
  
  public void retrieve(){
    
    this.passNext();
    
    if ( !this.soundIsPlaying ) {
      
      // Eliminate the pause
      if ( this.triggerCount < 0 ) {
        this.triggerCount++;
      }
      
      // Adjust the amplitude
      if ( this.focus != 0 ) { this.focus = 0; }
      
      // Grow the buffer or reset it
      if ( this.bufferIsOk() ) {
        this.triggerCount++;
      } else {
        this.triggerCount = 0;
      }
      
      // Process the trigger
      if ( this.triggerCount >= this.triggerLimit ) {
        this.soundIsPlaying = true;
        this.triggerCount = 0 - this.setting.getInt("pause");
      }
      
    } else {
      
      // Adjust the focus aspect
      this.mapFocus();
    
      // Play the song if not ended yet
      if ( this.soundLife < this.soundDuration ) {
        
        this.liveTheLifeOfTheSound();
      
      }
      // end the song if already ended
      else {
        this.soundIsPlaying = false;
        this.soundLife = 0;
      }
    
    }
    
    
  }
  
  public void mapFocus(){
    float focusMap = map( this.value, this.triggerMin, this.triggerMax,this.soundFocusedMin, this.soundFocusedMax);
    float focusLerp = lerp( focusMap, this.focus, 0.9 );
    this.focus = constrain( focusLerp, this.soundFocusedMin, this.soundFocusedMax );
  }
  
  public void liveTheLifeOfTheSound(){
    // this.soundLife++;
  }
  
  public void passNext(){
    
    this.value = connection.getFocal();
  
    for ( int i=0; i<this.triggerBuffer.length - 1;i++ ) {
      this.triggerBuffer[i+1] = this.triggerBuffer[i];
    }
    
    this.triggerBuffer[0] = this.value;
  
  }
  
  public boolean bufferIsOk(){
    boolean output = false;
    
    for ( float f : this.triggerBuffer ) {
      if ( f >= this.triggerMin && f <= this.triggerMax ) {
        output = true;
      }
    }
    
    return output;
  }
  
  public void process(){
  
    
    if ( c.debug() ) {
    
      pushMatrix(); push();
      
      translate(this.setting.getInt("debugX"),this.setting.getInt("debugY"));

    
      // The trigger
      float mappedCount = map( this.triggerCount, 0, this.triggerLimit, 0, 100 );
      
      noStroke();
      fill(0,255,255);
      text("Spouštěč zvuku: " + this.triggerCount, 0,20);
      
      rect(0, 30, mappedCount, 10);
      
      noFill();
      strokeWeight(1);
      stroke(0,255,255);
      
      rect(0, 30, mappedCount, 10);
      
      // The focus
      float mappedFocus = map( this.focus, 0, this.soundFocusedMax, 0, 100 );
      
      noStroke();
      fill(0,255,255);
      text("Focus zvuku: " + this.focus, 0,60);
      
      rect(0, 80, mappedFocus, 10);
      
      noFill();
      strokeWeight(1);
      stroke(0,255,255);
      
      rect(0, 80, mappedFocus, 10);
      
      // The duration
      float mappedDuration= map( this.soundLife, this.soundDuration, 0, 0, 100 );
      
      noStroke();
      fill(0,255,255);
      text("Trvání zvuku: " + this.soundLife + " z " + this.soundDuration, 0,100);
      
      rect(0, 120, mappedDuration, 10);
      
      noFill();
      strokeWeight(1);
      stroke(0,255,255);
      
      rect(0, 120, mappedDuration, 10);
      
      
      popMatrix(); pop();
    
    }
    
    
  }
  
}

class SongOutput extends SoundOutput {
  
  Sound sound;
  int numSamples;

  SongOutput( JSONObject obj ){
    super(obj);
    this.sound = new MusicalSound();
  }
  
  @Override
  public void mapFocus(){
    this.focus = map(this.value, this.triggerMin, this.triggerMax,this.soundFocusedMin, this.soundFocusedMax);
    this.focus = constrain( this.focus, this.soundFocusedMin, this.soundFocusedMax );
  }
  
  @Override
  public void liveTheLifeOfTheSound(){
    this.soundLife++;
  }
  
  public void process(){
    super.process();
    this.sound.update( this );
  }
  
  
}

class SynthOutput extends SoundOutput {
  
  Sound sound;

  SynthOutput( JSONObject obj ){
    super(obj);
    this.sound = new HeartSound();
  }
  
  @Override
  public void liveTheLifeOfTheSound(){

    if (this.bufferIsOk()) {
      this.soundLife = 0;
    } else {
      this.soundLife++;
    }

  }
  
  public void retrieve(){
    super.retrieve();
  }
  
  public void process(){
    super.process();
    this.sound.update( this );
  }
  
}

class DebugOutput extends Output {

  DebugValue infos[];
  
  DebugOutput( JSONObject obj ){
    super(obj);
    
    this.infos = new DebugValue[6];
    this.infos[0] = new DebugValue( "Světlo: centrum", color(255), "focalLight", c.lightMin(), c.lightMax() );
    this.infos[1] = new DebugValue( "Světlo: minimuim", color(255), "minLight", c.lightMin(), c.lightMax() );
    this.infos[2] = new DebugValue( "Světlo: maximum", color(255), "maxĹight", c.lightMin(), c.lightMax() );
    this.infos[3] = new DebugValue( "Totální centrum", color(0,255,255), "focal", c.totalMin(), c.totalMax() );
    this.infos[4] = new DebugValue( "Totální průměr", color(0,255,255), "average", c.totalMin(), c.totalMax() );
    this.infos[5] = new DebugValue( "Pan", color(255,0,0), "pan", -1, 1 );


  }
  
  public void retrieve(){}
  
  class DebugValue {
    
    String name, val;
    color col;
    float value, min, max;
    
    DebugValue( String name_, color col_, String val_, int min_, int max_ ){
      this.name = name_;
      this.col = col_;
      this.val = val_;
      this.min = (float) min_;
      this.max = (float) max_;
      this.value = this.min;
    }
    
    DebugValue( String name_, color col_, String val_, float min_, float max_ ) {
      this.name = name_;
      this.col = col_;
      this.val = val_;
      this.min = min_;
      this.max = max_;
      this.value = this.min;
    }
    
    public void pull(){
      switch ( this.val ) {
      
        case "focal":
          this.value = connection.getFocal(); break;
        case "focalLight":
          this.value = connection.getLightFocalFloat(); break;
        case "average":
          this.value = connection.getAverage(); break;
        case "averageLight":
          this.value = connection.getLightAverageFloat(); break;
        case "min":
          this.value = connection.getMin(); break;
        case "minLight":
          this.value = connection.getLightMinFloat(); break;
        case "max":
          this.value = connection.getMax(); break;
        case "maxĹight":
          this.value = connection.getLightMaxInt(); break;
        case "pan":
          this.value = connection.getPan(); break;
        
      }
    }
    
    public void render() {
      
      this.pull();
      
      push();
      
      noStroke();
      fill( this.col );
      
      text( this.name + ": " + this.value + " (" + this.min + " - " + this.max + ")", 0,20 );
      rect( 0, 20, map(this.value, this.min, this.max, 0,255), 10 );
      
      noFill();
      stroke( this.col );
      rect( 0, 20, map(this.value, this.min, this.max, 0,255), 10 );
       
      pop();
    }
  
  }
  
  
  public void process(){
  
    pushMatrix(); push();
    
    translate(this.setting.getInt("debugX"),this.setting.getInt("debugY"));
    
    int i=0; int h = 30;
    for ( DebugValue v : this.infos ) {
      
      pushMatrix();
    
      translate(0,i*h);
      
      v.render();
      i++;
    
      popMatrix();
      
    }
    
    popMatrix(); pop();
  }
  
}
