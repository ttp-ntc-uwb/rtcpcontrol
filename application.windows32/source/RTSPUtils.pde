class RTSPState {
  
  private int cseq;
  private boolean processed;
  
  // Connection details 
  private String serverIP;
  private int serverPort;
  private String serverURI;
  
  // Message content
  private String header;
  private StringDict msg;
  private String message;

  RTSPState( int cseq_, String ip_, int port_, String header_ ){
  
    // State attributes
    this.cseq = cseq_;
    this.processed = false;
    
    // Connection details
    this.serverIP = ip_;
    this.serverPort = port_;
    
    // Message content
    this.header = header_;
    this.msg = new StringDict();
    
  }
  
  // Helpers
  
  // Render the message
  void renderMessage(){
  
    // First line - header info
    this.message = this.header + " rtsp://" + this.serverIP + ":" + this.serverPort;
    
    // If an URI is present, add that
    if ( this.serverURI != null ) {
      this.message += "/"+this.serverURI;
    }
    
    this.message += " RTSP/1.0";
    
    // Second line - CSeq
    this.message += "\nCSeq: " + this.cseq + "\n";
    
    
    // Third line and more - optional parameters
    if ( this.msg.size()>0 ) {
    
      for ( String k : this.msg.keyArray() ) {
        
        this.message += k + ": " + this.msg.get(k) + "\n";
        
      }
    
    }
  
  }
  
  // Getters
  public boolean isProcessed(){
    return this.processed;
  }
  public int cseq(){
    return this.cseq;
  }
  public String getMessage() {
    this.renderMessage();
    return this.message;
  }
  
  // Setters
  
  // Set the processed state
  public void process(){
    this.processed = true;
  }
  
  // Set the source uri
  public void setURI( String uri ){
    this.serverURI = uri;
  }
  
  // Add an option to the message
  public void setLine(String name, String value){
    this.msg.set(name,value);
  }
  
  public void setCseq( int i ){
    this.cseq = i;
  }
  public void increaseCseq(){
    this.cseq++;
  }
  
}

static class RTCPParser {

  RTCPParser(){}
  
  /* Parse the message Cseq */
  public static int cseq( String msg ){
  
    int o = -2;
    
    msg = msg.substring( msg.indexOf("CSeq: ") + 1 );
    msg = msg.substring( 5, msg.indexOf("\n") - 1 );
    
    o = Integer.parseInt(msg);
    
    return o;
  }
  
  public static int status( String msg ){
    
    String rows[] = msg.split("\n");
    
    int status = 0;
    
    if ( rows.length > 0 ) {
    
      String words[] = rows[0].split(" ");
      status = Integer.parseInt( words[1] );
    
    }
    
    return status;
    
  }
  
  String[] splitByRows( String msg ){
    return msg.split("\n");
  }
  
  public static String getStringValueAt( String attribute, String msg ){
  
    String output = null;
    
    String rows[] = msg.split("\n");
    
    for ( String row : rows ) {
      if ( row.contains(attribute) ) {
        output = row.substring( attribute.length() + 2 );
      }
    }
    
    return output;
  
  }

}
