import ddf.minim.javasound.*;

abstract class Sound {
  
  Instrument inst;

  Sound(){}
  
  abstract public void start();
  
  abstract public void update( SoundOutput controller );
  
  abstract public void end();

}

class Song extends Sound {

  Song(){
    super();
  }
  
  public void start(){}
  
  public void update( SoundOutput controller ){
  
  }

  public void end(){}
  
}

class FMSound extends Sound {
  
  Oscil osc, modulator;
  Balance bal;
  float freq, cf;
  float amp, ampMax;

  FMSound(){
    super();
    this.cf = 80;
    this.freq = this.cf;
    this.amp = 0.0f;
    this.ampMax = 0.5;
    this.start();
  }
  
  public void start(){
    this.osc = new Oscil( this.freq, this.amp, Waves.SINE );
    this.modulator = new Oscil( 10, 2, Waves.SINE );
    this.modulator.offset.setLastValue(this.freq);
    this.modulator.patch( this.osc.frequency );
    
    this.bal = new Balance(0f);
    this.osc.patch( dac );
  }
  
  public void update( SoundOutput controller ){
    
    // Process the amplitude;
    if ( controller.soundIsPlaying && this.amp < this.ampMax ) {
      this.amp += 0.05;
    }
    if ( ! controller.soundIsPlaying && this.amp > 0) {
      this.amp -= 0.01;
    }
    this.osc.setAmplitude( this.amp );
    
    // FM
    // if ( frameCount % 1 == 0) {
      this.freq = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 0.5,100f );
      this.modulator.setFrequency( this.freq  );
      
      float modulateAmp = map( connection.getLightAverageFloat(), c.lightMin(),c.lightMax(), this.cf, 1 );
      this.modulator.setAmplitude( modulateAmp * this.amp );
      
      // float amplitude = map( controller.focus, 0,1,0,0.5);
      // this.osc.setAmplitude( this.amp );
      // dac.setGain( amplitude );
      
      // float bla = constrain( connection.getPan(), -1f, 1f  );
      // this.bal.setBalance( bla );
    // }
    
  }

  public void end(){}
  
}

class BlipSound extends Sound {
  
  Oscil osc;
  BandPass bpf;
  Balance bal;
  float freq;
  float amp, maxAmp;

  BlipSound(){
    super();
    this.freq = 3;
    this.amp = 0f;
    this.maxAmp = 1f;
    this.start();
  }
  
  public void start(){
    this.osc = new Oscil( this.freq, this.amp, Waves.SAW );
    this.bpf = new BandPass( 100, 20, dac.sampleRate() );
    this.bal = new Balance( 0 );
    this.osc.patch( this.bpf ).patch( this.bal ).patch( dac );
  }
  
  public void update( SoundOutput controller ){

    // Process the amplitude;
    if ( controller.soundIsPlaying && this.amp < this.maxAmp ) {
      this.amp += 0.05;
    }
    if ( ! controller.soundIsPlaying && this.amp > 0) {
      this.amp -= 0.01;
    }
    this.osc.setAmplitude( this.amp );
    
    // BPF & SAWTOOTH
    // if ( frameCount % 1 == 0) {
      
      // Update the frequency based on light focal range
      this.freq = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 1,20f );
      this.osc.setFrequency( this.freq  );
      
      // Update the BPF based on light average value
      float average = (float) map( connection.getLightAverageFloat(), c.lightMin(), c.lightMax(), 20f, 100f );
      this.bpf.setBandWidth( average );
      
      // Update the pan based on the pan
      float bla = constrain( connection.getPan(), -1f, 1f  );
      this.bal.setBalance( bla );
    // }
    
  }

  public void end(){}
  
}

class InstrumentSound extends Sound {
  
  int triggerCounter;
  int triggerLimit;
  int freqMin, freqMax, nextFreq, freqCounter;

  InstrumentSound() {
    super();
    
    this.triggerCounter = 0;
    this.triggerLimit = 5;
    
    this.freqMin = 30; 
    this.freqMax = 180; 
    
    this.generateNextStep();
    
    this.freqCounter = 0;
    
    this.start();
    
  }
  
  private void generateNextStep() {
    this.nextFreq = (int) random( this.freqMin, this.freqMax );
  }
  
  @Override
  public void start(){}
  
  @Override 
  public void end() {}
  
  @Override
  public void update( SoundOutput controller ) {
  
    if ( !isSinging ) {
    
      if ( connection.getFocal() > c.songMin() ) {
      
        this.triggerCounter++;
      
      } else {
        this.triggerCounter = 0;
      }
      
      if ( this.triggerCounter > this.triggerLimit ) {
      
        isSinging = true;
        this.triggerCounter = 0;
        
        this.playSequence();
        
      }
      
    } else {
    
      // if ( this.freqCounter > this.nextFreq ) {
      if ( this.freqCounter > 240 ) {
      
        this.generateNextStep();
        this.freqCounter = 0;
        
        this.playSequence();
        
        
      } else {
        
        this.freqCounter++;
        
      }
      
    }
    
    //  Check if there is a hogher point in the radius otherwise delete it
    if ( connection.getMax() < c.songMin() ) {
      isSinging = false;
    }
  
  }
  
  private void playSequence(){
    
    int dice = (int) random(0,3);
    int baseSeqNum = (int) map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 1, pam.buffers.length-2 );
    
    float pause = 0.5f;
    
    int seq[] = new int[4];
    
    switch ( dice ) {
    
      case 0:
        seq = new int[4];
        seq[0] = 0;
        seq[1] = -1;
        seq[2] = -1;
        seq[3] = 0;
        break;
        
      case 1:
        seq = new int[4];
        seq[0] = 0;
        seq[1] = 1;
        seq[2] = 0;
        seq[3] = -1;
        break;
        
      case 3:
        seq = new int[4];
        seq[0] = 0;
        seq[1] = -1;
        seq[2] = 0;
        seq[3] = 1;
        break;
        
    }
    
    dac.playNote( 0,    1,  new SampleInstrument( baseSeqNum + seq[0], pam ));
    dac.playNote( 0.75, 1,  new SampleInstrument( baseSeqNum + seq[1], pam ));
    dac.playNote( 1,    1,  new SampleInstrument( baseSeqNum + seq[2], pam ));
    dac.playNote( 1.5,  1,  new SampleInstrument( baseSeqNum + seq[3], pam ));
  
  }
  
}

class MusicalSound extends Sound {
  
  int triggerCounter;
  int triggerLimit;
  int freqMin, freqMax, nextFreq, freqCounter, chantCounter, chantLimit;
  PImage scale;
  color col;
  int intensity;

  MusicalSound() {
    super();
    
    this.triggerCounter = 0;
    this.triggerLimit = 5;
    
    this.freqMin = 360; 
    this.freqMax = 600; 
    this.chantCounter = 0;
    
    this.generateNextStep();
    
    this.freqCounter = 0;
    this.chantLimit = 3;
    
    this.intensity = 0;
    
    this.col = color(0);
    
    this.scale = loadImage( "head.jpg" );
    
    this.start();
    
  }
  
  private void generateNextStep() {
    this.nextFreq = (int) random( this.freqMin, this.freqMax );
  }
  
  @Override
  public void start(){}
  
  @Override 
  public void end() {}
  
  @Override
  public void update( SoundOutput controller ) {
    
    
    if ( isSinging ) {
        
        this.col = lerpColor( this.col, color(255,148,0), 0.33 );
        
    }
    
    if ( !isSinging ) {
        
        this.col = lerpColor( this.col, color(0), 0.33 );
        
    }
    
    if ( c.debug() ) {
      
      pushMatrix(); push();
      
      translate(300,height/2);
      
      fill(this.col);
      rect(0, 0, 100, 100);
      
      popMatrix(); pop();
    
    }
  
    if ( !isSinging ) {
    
      if ( connection.getFocal() > c.songMin() ) {
      
        this.triggerCounter++;
      
      } else {
        this.triggerCounter = 0;
      }
      
      if ( this.triggerCounter > this.triggerLimit ) {
      
        isSinging = true;
        this.triggerCounter = 0;
        
        this.playSong();
        
      }
      
    } else {
    
      if ( this.freqCounter > this.nextFreq ) {
      // if ( this.freqCounter > 240 ) {
      
        this.generateNextStep();
        this.freqCounter = 0;
        this.chantCounter = 0;
        
        this.playSong();
        
        
      } else {
        
        this.freqCounter++;
        
      }
      
    }
    
    //  Check if there is a hogher point in the radius otherwise delete it
    if ( connection.getMax() < c.songMin() || this.chantCounter > this.chantLimit ) {
      isSinging = false;
    }
  
  }
  
  private void playSong(){
    
    this.chantCounter++;
    
    int dice = (int) random(0,songs.buffers.length);
    
    dac.playNote( 0,    this.getAmplitude(),  new SampleInstrument( dice, songs ));
    // println( numChantsSinging );
  
  }
  
  float getAmplitude() {
  
    float output = 0.0f;
    
    switch ( this.chantCounter ) {
    
      case 0:
        output = -12f; break;
      case 1:
        output = -30f; break;
      case 3:
        output = -6f; break;
      case 4:
        output = -12f; break;
      case 5:
        output = -18f; break;
      case 6:
        output = -30f; break;
      case 7:
        output = -20f; break;
      default:
        output = -12f; break;
    
    }
    
    return output;
    
  }
  
}

class SampleSound extends Sound {

  Sampler samples[];
  float amplitude;
  float frequency;
  int frequencyMin, frequencyMax, frequencyCounter, index;
  int overallTime, overallLimit, trigger, triggerLimit;
  boolean isPlaying;
  
  SampleSound(){
  
    super();
    
    this.samples = new Sampler[8];
    
    this.frequencyMin = 300;
    this.frequencyMax = 30;
    this.frequencyCounter = 0;
    
    this.overallTime = 0;
    this.overallLimit = 400;
    
    this.isPlaying = false;
    this.trigger = 0;
    this.triggerLimit = 3;
    
    this.start();
    
  }
  
  public void start(){
  
    for ( int i = 1; i < 8; i++ ) {
      
      String fileName = "data/pim/0"+i+".mp3";
      this.samples[i] = new Sampler( fileName, 1, minim );
      
      this.samples[i].patch( dac );
  
    }
    
  }
  
  public void update( SoundOutput controller ){
    
    // check if the maximum is over headLimit
    if ( !this.isPlaying && connection.getFocal() > 43f ) {
      if ( this.trigger < this.triggerLimit ) {
        this.trigger++;
      } else {
        this.isPlaying = true;
        this.trigger=0;
        isSinging = true;
      }
    }
    
    if ( this.isPlaying ) {
    
      this.overallTime++;
      
      if ( this.overallTime < this.overallLimit ) {
        
        // Do the work now
        this.frequency = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), this.frequencyMax, this.frequencyMin );
        this.frequency = 60;
        
        if ( this.frequencyCounter < this.frequency ) {
          
          this.frequencyCounter++;
        
        } else {
          
          this.index = (int) map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 0, 7 );
          
          this.samples[index].trigger();
          
          this.amplitude = map( connection.getFocal(), c.totalMin(), c.totalMax(), 0, 1 );
        
          this.frequencyCounter = 0;
          
        }
      
      } else {
        this.isPlaying = false;
        isSinging = true;
      }
    
    }
  
  }
  
  public void end(){}
  
}


class HeartSound extends Sound {
  
  FilePlayer player;
  BandPass bpf;
  Balance bal;
  Gain gain;
  float freq;
  float amp, maxAmp;
  int life, next;

  HeartSound(){
    super();
    this.freq = 3;
    this.amp = 0f;
    this.maxAmp = 2f;
    this.life = 0;
    this.next = 60;
    this.start();
  }
  
  public void start(){
    this.player = new FilePlayer( minim.loadFileStream("heart_clear.wav") );
    this.bal = new Balance( 0 );
    this.gain = new Gain( 0f );
    this.bpf = new BandPass( 200, 40, dac.sampleRate() );
    
    this.player.play();
    
    this.player.patch( this.bpf ).patch( this.bal ).patch( this.gain ).patch( dac );
  }
  
  public void update( SoundOutput controller ){
    
    this.life++;
    
    if ( this.life >= this.next ) {
      if ( heartAspect > 0.1 ) {
        
        this.player.rewind();
        this.player.play();
        this.life = 0;
        
      }
      
    }
    
    if (isSinging && heartAspect > 0.1) {
    
      heartAspect -= 0.05;
    
    }
    
    if ( !isSinging && heartAspect < 1 ) {
      
      heartAspect += 0.05;
      
    }
    
    this.next = (int) map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 150, 40 );

    // Process the amplitude;
    if ( controller.soundIsPlaying && this.amp < this.maxAmp ) {
      this.amp += 0.05;
    }
    
    if ( ! controller.soundIsPlaying && this.amp > 0) {
      this.amp -= 0.01;
    }
    
    this.amp = this.amp > 1 ? 1 : this.amp; 
    this.amp = this.amp < 0 ? 0 : this.amp;
    
    this.gain.setValue( map( this.amp,0,1,-60,0) * heartAspect );
      
      // Update the frequency based on light focal range
      this.freq = map( connection.getLightFocalFloat(), c.lightMin(), c.lightMax(), 0.5,5f );
      
      // Update the BPF based on light average value
      float average = (float) map( connection.getLightAverageFloat(), c.lightMin(), c.lightMax(), 30f, 100f );
      this.bpf.setBandWidth( average );
      
      float bpfFreq = map( connection.getLightAverageFloat(), c.lightMin(), c.lightMax(), 80, 200 );
      
      this.bpf.setFreq( bpfFreq );
      
      // Update the pan based on the pan
      float bla = constrain( connection.getPan(), -1f, 1f  );
      this.bal.setBalance( bla );
    
  }

  public void end(){}
  
}
